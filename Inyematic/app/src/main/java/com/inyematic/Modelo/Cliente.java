package com.inyematic.Modelo;

import java.io.Serializable;

public class Cliente implements Serializable {
    private long id;
    private String name;
    private String address;
    private long telephone;

    public Cliente() {
    }

    @Override
    public String toString() {
        return name;
    }

    public Cliente(long id, String name, String address, long telephone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.telephone = telephone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTelephone() {
        return telephone;
    }

    public void setTelephone(long telephone) {
        this.telephone = telephone;
    }
}
