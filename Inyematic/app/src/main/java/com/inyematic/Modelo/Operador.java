package com.inyematic.Modelo;

import java.io.Serializable;
import java.util.Date;

public class Operador implements Serializable {
    private long id;
    private String name;
    private String address;

    private Date birthDate;

    public Operador(long id, String name, String address, Date birthDate) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return name;
    }

    public Operador() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}
