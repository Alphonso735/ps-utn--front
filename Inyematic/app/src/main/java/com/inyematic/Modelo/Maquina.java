package com.inyematic.Modelo;

import java.io.Serializable;

public class Maquina implements Serializable {
    private long id;
    private String name;
    private String alias;
    private int hopperSize;

    public Maquina() {
    }

    @Override
    public String toString() {
        return  name ;
    }

    public Maquina(long id, String name, String alias, int hopperSize) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.hopperSize = hopperSize;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getHopperSize() {
        return hopperSize;
    }

    public void setHopperSize(int hopperSize) {
        this.hopperSize = hopperSize;
    }
}
