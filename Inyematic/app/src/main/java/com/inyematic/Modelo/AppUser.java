package com.inyematic.Modelo;

import java.io.Serializable;

public class AppUser implements Serializable {

    private long id;
    private String userName;
    private String password;
    private EntryLevel entryLevel;




    public EntryLevel getEntryLevel() {
        return entryLevel;
    }

    public void setEntryLevel(EntryLevel entryLevel) {
        this.entryLevel = entryLevel;
    }

    public AppUser() {
    }

    public AppUser(long id, String userName, String password,EntryLevel entryLevel) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.entryLevel=entryLevel;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
