package com.inyematic.Modelo.dto;

import java.util.Date;

public class DatesTransfer {
    private Date dateA;
    private Date dateB;

    public DatesTransfer() {
    }

    public DatesTransfer(Date dateA, Date dateB) {
        this.dateA = dateA;
        this.dateB = dateB;
    }

    public Date getDateA() {
        return dateA;
    }

    public void setDateA(Date dateA) {
        this.dateA = dateA;
    }

    public Date getDateB() {
        return dateB;
    }

    public void setDateB(Date dateB) {
        this.dateB = dateB;
    }
}
