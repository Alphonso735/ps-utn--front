package com.inyematic.Modelo;

import java.io.Serializable;

public class Molde implements Serializable {
    private int id;
    private String name;
    private float weight;
    private Material material;

    @Override
    public String toString() {
        return name;
    }

    public Molde(int id, String name, float weight, Material material) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.material=material;
    }
    public Molde(String name, float weight,Material material) {

        this.name = name;
        this.weight = weight;
        this.material=material;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
