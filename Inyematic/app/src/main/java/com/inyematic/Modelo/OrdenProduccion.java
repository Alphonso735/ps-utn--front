package com.inyematic.Modelo;

import java.io.Serializable;

public class OrdenProduccion implements Serializable {
    private long id;
    private Molde mould;
    private Operador operator;
    private Maquina machine;
    private int pieceCount;
    private StatusPO status;
    private Cliente client;

    public Cliente getCliente() {
        return client;
    }

    public void setCliente(Cliente cliente) {
        this.client = cliente;
    }

    public OrdenProduccion() {
    }

    @Override
    public String toString() {
        return "OrdenProduccion{" +
                "id=" + id +
                ", mould=" + mould +
                ", operator=" + operator +
                ", machine=" + machine +
                ", pieceCount=" + pieceCount +
                ", status=" + status +
                ", client=" + client +
                '}';
    }

    public OrdenProduccion(long id, Molde mould, Operador operator, Maquina machine, int pieceCount, StatusPO status, Cliente cliente) {
        this.id = id;
        this.mould = mould;
        this.operator = operator;
        this.machine = machine;
        this.pieceCount = pieceCount;
        this.status = status;
        this.client=cliente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Molde getMould() {
        return mould;
    }

    public void setMould(Molde mould) {
        this.mould = mould;
    }

    public Operador getOperator() {
        return operator;
    }

    public void setOperator(Operador operator) {
        this.operator = operator;
    }

    public Maquina getMachine() {
        return machine;
    }

    public void setMachine(Maquina machine) {
        this.machine = machine;
    }

    public int getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(int pieceCount) {
        this.pieceCount = pieceCount;
    }

    public StatusPO getStatus() {
        return status;
    }

    public void setStatus(StatusPO status) {
        this.status = status;
    }
}
