package com.inyematic.Modelo;

import java.io.Serializable;

public class Caja implements Serializable {
    private long id;
    private int numOfItems;

    public Caja() {
    }

    public Caja(long id, int numOfItems) {
        this.id = id;
        this.numOfItems = numOfItems;
    }

    @Override
    public String toString() {
        return "Caja{" +
                "id=" + id +
                ", numOfItems=" + numOfItems +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumOfItems() {
        return numOfItems;
    }

    public void setNumOfItems(int numOfItems) {
        this.numOfItems = numOfItems;
    }
}
