package com.inyematic.Modelo;

import java.io.Serializable;

public class StatusPO implements Serializable {
    private String name;
    private long id;

    public StatusPO() {
    }

    public StatusPO(String name, long id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return  name ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
