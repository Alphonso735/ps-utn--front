package com.inyematic.Modelo;

import java.io.Serializable;
import java.util.Date;

public class EntradaMaterial implements Serializable {

    private long id;
    private Material material;
    private Date date;
    private int numOfBags;
    private boolean status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNumOfBags() {
        return numOfBags;
    }

    public void setNumOfBags(int numOfBags) {
        this.numOfBags = numOfBags;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public EntradaMaterial() {
    }

    public EntradaMaterial(long id, Material material, Date date, int numOfBags, boolean status) {
        this.id = id;
        this.material = material;
        this.date = date;
        this.numOfBags = numOfBags;
        this.status = status;
    }
}
