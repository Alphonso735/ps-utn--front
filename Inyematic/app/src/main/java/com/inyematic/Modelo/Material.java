package com.inyematic.Modelo;

import java.io.Serializable;

public class Material implements Serializable {
    private int id;
    private String name;
    private float numOfKil;

    public float getNumOfKil() {
        return numOfKil;
    }

    public void setNumOfKil(float numOfKil) {
        this.numOfKil = numOfKil;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Material(int id, String name, float numOfKil) {
        this.id = id;
        this.name = name;
        this.numOfKil=numOfKil;
    }
    public Material (){}
}
