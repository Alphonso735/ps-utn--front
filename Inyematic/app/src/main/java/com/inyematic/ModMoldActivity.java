package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Interfaz.MouldApi;
import com.inyematic.Modelo.Material;
import com.inyematic.Modelo.Molde;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModMoldActivity extends AppCompatActivity {
    Molde moldeSelected;
    //-------------
    TextView moldeNombre,lblMatSelected;
    EditText txtNombre;
    EditText txtPeso;
    Spinner spMat;
    //------------
    ArrayList<Material> materiales=new ArrayList<>();
    Material matSelected;
    //----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_mold);
        //*******Enlazo los view************************
        moldeNombre=(TextView)findViewById(R.id.moldeModificar);
        lblMatSelected=(TextView)findViewById(R.id.lblMatSelected);
        spMat=(Spinner)findViewById(R.id.spinnerMateriales);
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtPeso=(EditText)findViewById(R.id.txtPeso);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //-----------Los Inicializo-----------
        this.getMolde();

        this.loadMoldeData();

        this.cargarMateriales();

    }

    private void cargarSpinner() {
        //*****Aca cargo el spinner con los datos del Array materiales
        ArrayAdapter<CharSequence> adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,materiales);
        spMat.setAdapter(adapter);
        //------pongo el spinner en la opcion del molde
        int posMat=-1;
        for(Material m:materiales)
            if(m.getId()==matSelected.getId())
                posMat=materiales.indexOf(m);
        spMat.setSelection(posMat);
        //******Aca manejo la selección de un material dentro del spinner
        spMat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),
                        "Seleccionado: "+parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                matSelected=(Material)parent.getItemAtPosition(position);
                lblMatSelected.setText(matSelected.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarMateriales() {
        //******Cargo el array materiales****
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MaterialApi materialApi=retrofit.create(MaterialApi.class);

        Call<List<Material>> call=materialApi.getAllMaterials();

        call.enqueue(new Callback<List<Material>>() {
            @Override
            public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"Fallo en el Response",Toast.LENGTH_SHORT).show();
                    return;
                }
                //*******
                List<Material> lstMaterials=response.body();

                for(int i=0;i<lstMaterials.size();i++){
                    materiales.add(lstMaterials.get(i));
                }
                //cargo por el asincrono
                cargarSpinner();
            }

            @Override
            public void onFailure(Call<List<Material>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Fallo",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMoldeData() {
        //***Aca cargo los datos del molde a los view*********
        moldeNombre.setText(moldeSelected.getName());
        txtNombre.setText(moldeSelected.getName());
        txtPeso.setText(String.valueOf(moldeSelected.getWeight()));
    }

    private void getMolde() {
        //***Aca obtengo el molde seleccionado desde el intent que genero la Activity
        Bundle mibundle=this.getIntent().getExtras();
        moldeSelected= (Molde) mibundle.getSerializable("molde");
        matSelected=moldeSelected.getMaterial();
    }

    public void onClick(View view) {
        setResult(1);
        finish();
    }

    public void cargarCambios(View view) {
        //------Este seria el molde con los nuevos datos----------------
        Molde newMolde=new Molde( moldeSelected.getId(),
                txtNombre.getText().toString(),
                Float.valueOf(txtPeso.getText().toString()),
                matSelected
        );

        //**************************
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MouldApi mouldApi=retrofit.create(MouldApi.class);

        Call<ResponseBody> call=mouldApi.updateMould(newMolde);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                int codigo=response.code();

                switch (codigo){
                    case 200:
                        Toast.makeText(getApplicationContext(), "Se guardo ", Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(), "Error en los datos", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error en la conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }
}//end
