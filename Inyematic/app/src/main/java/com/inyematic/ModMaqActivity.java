package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Modelo.Maquina;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModMaqActivity extends AppCompatActivity {
    //----------
    private String URL;
    //------------------
    EditText txtNombre,txtAlias,txtTolva;
    //----------
    Maquina maquina;
    //----------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_maq);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtAlias=(EditText)findViewById(R.id.txtAlias);
        txtTolva=(EditText)findViewById(R.id.txtTolva);
        //------------------
        obtenerMaquina();
        cargarDatos();
    }

    private void cargarDatos() {
        txtNombre.setText(maquina.getName());
        txtAlias.setText(maquina.getAlias());
        txtTolva.setText(String.valueOf(maquina.getHopperSize()));
    }

    private void obtenerMaquina() {
        Bundle bundle=getIntent().getExtras();
        maquina=(Maquina)bundle.getSerializable("maquina");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardarCambios(View view) {
        Maquina nuevaMaq=new Maquina();
        nuevaMaq.setName(txtNombre.getText().toString());
        nuevaMaq.setAlias(txtAlias.getText().toString());
        nuevaMaq.setId(maquina.getId());

        try{
            String tamanio=txtTolva.getText().toString();
            int tamInt=Integer.parseInt(tamanio);
            nuevaMaq.setHopperSize(tamInt);
        }catch (Exception e){
            nuevaMaq.setHopperSize(0);
        }
        //------------------------------
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MaquinaApi maquinaApi=retrofit.create(MaquinaApi.class);

        //------------------------------

        Call<ResponseBody> call=maquinaApi.updateMachine(nuevaMaq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se guardaron los cambios",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se guardaron los cambios",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}//end
