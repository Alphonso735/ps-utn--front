package com.inyematic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MenuJefe extends AppCompatActivity {

    private int NivelAcceso;
    private String URL;
    //--------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_jefe);
        //-------------------------------

        //-------------------------------
        obtenerDatosIntent();
    }

    private void obtenerDatosIntent() {
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",NivelAcceso);


        //------------------

    }

    public void CerrarMenuJefe(View view) {
        finish();
    }

    public void llamarWinMateriales(View view) {
        Intent ventana=new Intent(this,MaterialesActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinMoldes(View view) {
        Intent ventana=new Intent(this,MoldesActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinMaquinas(View view) {
        Intent ventana=new Intent(this,MaquinasActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinClientes(View view) {
        Intent ventana=new Intent(this,ClientesActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinBolsasMaterial(View view) {
        Intent ventana=new Intent(this,BolsasMaterialActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinOperadores(View view) {
        Intent ventana=new Intent(this,OperadoresActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }

    public void llamarWinOrdenesP(View view) {
        Intent ventana=new Intent(this,OrdenesPActivity.class);
        ventana.putExtra("URL",URL);
        ventana.putExtra("NivelAcceso",NivelAcceso);
        startActivity(ventana);
    }

    public void llamarWinCuentas(View view) {
        Intent ventana=new Intent(this,CuentasActivity.class);
        ventana.putExtra("URL",URL);
        startActivity(ventana);
    }
}//end
