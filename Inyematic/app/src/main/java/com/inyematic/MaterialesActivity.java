package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosMateriales;
import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Modelo.Material;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MaterialesActivity extends AppCompatActivity {
    RecyclerView recycler;
    //-----------------------
    ArrayList<Material> listaMateriales=new ArrayList<>();
    Material materialSelec=null;
    //-------------------------
    private String URL;
    //-------------------------
    TextView selectPosRecy=null;
    //-------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materiales);
        //------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        //--------------------------
        obtenerDatosIntent();
        //--------------------------
        obtenerMateriales();
    }

    private void obtenerDatosIntent() {
        URL=getIntent().getStringExtra("URL");
    }

    private void obtenerMateriales() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MaterialApi materialApi=retrofit.create(MaterialApi.class);
        //-----------------------------------
        Call<List<Material>> call=materialApi.getAllMaterials();

        call.enqueue(new Callback<List<Material>>() {
            @Override
            public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                if(response.isSuccessful()){
                    List<Material> lista=response.body();

                    for(Material m:lista)
                        listaMateriales.add(m);

                    cargarRecycler();
                }else
                    Toast.makeText(getApplicationContext(),
                            "Error en obtener los datos",
                            Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Material>> call, Throwable t) {

                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosMateriales adapter=new AdapterDatosMateriales(this.listaMateriales);
        //----------------------
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+
                                listaMateriales.get(recycler.getChildAdapterPosition(v)).getName(),
                        Toast.LENGTH_LONG).show();
                materialSelec=listaMateriales.get(recycler.getChildAdapterPosition(v));
                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------
            }
        });

        //-----------------------------
        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    public void actionBorrar(View view) {
        if(materialSelec!=null){
           Intent winBorrar=new Intent(this,BorrMatActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("material",materialSelec);
            winBorrar.putExtras(bundle);
            winBorrar.putExtra("URL",URL);
            startActivityForResult(winBorrar,1);
            materialSelec=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void actionModificar(View view) {
        if(materialSelec!=null){
              Intent winModificar=new Intent(this,ModMatActivity.class);
              Bundle bundle=new Bundle();
              bundle.putSerializable("material",materialSelec);
              winModificar.putExtras(bundle);
              winModificar.putExtra("URL",URL);
              startActivityForResult(winModificar,1);
              materialSelec=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        recargarMats();
    }

     private void recargarMats(){
        listaMateriales.clear();
        obtenerMateriales();
    }
    public void actionAgregar(View view) {
       Intent winAgregar=new Intent(this,AgMatActivity.class);
       winAgregar.putExtra("URL",URL);
       startActivityForResult(winAgregar,1);
    }

    public void actionRegresar(View view) {
        finish();
    }
}//end
