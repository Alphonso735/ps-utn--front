package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.inyematic.Interfaz.AppUserApi;
import com.inyematic.Interfaz.EntryLevelApi;
import com.inyematic.Modelo.AppUser;
import com.inyematic.Modelo.EntryLevel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModCuenActivity extends AppCompatActivity {
    RadioGroup radioCuenta;
    EditText txtUSName,txtPass;
    //----------------------
    private String URL;
    //----------------------
    ArrayList<EntryLevel> listaLevel=new ArrayList<>();
    ///-----------------
    AppUser appUser=null;
    ///-----------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_cuen);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        radioCuenta=(RadioGroup)findViewById(R.id.radioCuenta);
        txtPass=(EditText)findViewById(R.id.txtPass);
        txtUSName=(EditText)findViewById(R.id.txtUSName);
        //------------------------
        obtenerLevel();
        //-----------
        appUser=(AppUser) getIntent().getSerializableExtra("cuenta");
        cargarCampos();
    }

    private void cargarCampos() {
        txtUSName.setText(appUser.getUserName());
        txtPass.setText(appUser.getPassword());
        long radio=appUser.getEntryLevel().getId();
        if(radio==1L)
            radioCuenta.check(R.id.rbtnJefe);
        if(radio==2L)
            radioCuenta.check(R.id.rbtnEncargado);
        if(radio==3L)
            radioCuenta.check(R.id.rbtnOperario);
    }

    private void obtenerLevel(){
        Retrofit retrofit=obtenerRetro();
        EntryLevelApi entryLevelApi=retrofit.create(EntryLevelApi.class);

        Call<List<EntryLevel>> call=entryLevelApi.getAllEntryLevel();

        call.enqueue(new Callback<List<EntryLevel>>() {
            @Override
            public void onResponse(Call<List<EntryLevel>> call, Response<List<EntryLevel>> response) {
                List<EntryLevel> lista=response.body();
                for(EntryLevel el:lista)
                    listaLevel.add(el);
            }

            @Override
            public void onFailure(Call<List<EntryLevel>> call, Throwable t) {

            }
        });
    }
    private Retrofit obtenerRetro() {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionModificar(View view) {
            AppUser nuevo=new AppUser();
            nuevo.setId(appUser.getId());
            nuevo.setUserName(txtUSName.getText().toString());
            nuevo.setPassword(txtPass.getText().toString());
            EntryLevel el=null;
            if(radioCuenta.getCheckedRadioButtonId()==R.id.rbtnJefe)
                el=listaLevel.get(0);
            if(radioCuenta.getCheckedRadioButtonId()==R.id.rbtnEncargado)
                el=listaLevel.get(1);
            if(radioCuenta.getCheckedRadioButtonId()==R.id.rbtnOperario)
                el=listaLevel.get(2);
            nuevo.setEntryLevel(el);
        //--------------------------------
        Retrofit retrofit=obtenerRetro();
        AppUserApi appUserApi=retrofit.create(AppUserApi.class);

        Call<ResponseBody>call=appUserApi.updateAppUser(nuevo);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se guardo la cuenta",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se guardo la cuenta",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}//end
