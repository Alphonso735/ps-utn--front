package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.AdapterDatos.AdapterDatosOrdenesP;
import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.Modelo.StatusPO;
import com.inyematic.Modelo.dto.DatesTransfer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsultasOrdenesActivity extends AppCompatActivity {

    private String URL;
    private int NivelAcceso;
    private long idCliente;
    //----------------------
    RecyclerView recycler;
    //----------------------
    Date dateA,dateB;
    //----------------------
    TextView lblFecOpDesde,lblFecOpHasta;
    //----------------------
    ArrayList<OrdenProduccion> listaOrdenes=new ArrayList<>();
    OrdenProduccion ordenSelect;
    //----------------------
    Spinner spEstados;
    ArrayList<StatusPO>lstEstados=new ArrayList<>();
    ArrayList<Integer>listaCantidadReal=new ArrayList<>();
    StatusPO estadoSelec;
    //----------------------
    TextView selectPosRecy=null;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultas_ordenes);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",0);
        idCliente= getIntent().getLongExtra("idCliente",0);
        //---------------------------------
        recycler=(RecyclerView) findViewById(R.id.lstConsultaOp);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        //---------------------------------
        lblFecOpDesde=(TextView)findViewById(R.id.lblFecOpDesde);
        lblFecOpHasta=(TextView)findViewById(R.id.lblFecOpHasta);
        //---------------------------------
        spEstados=(Spinner)findViewById(R.id.spEstados);
        //---------------------------------
        obtenerListadoEstados();
        inicializarFechas();
    }
    private void obtenerListadoEstados() {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<List<StatusPO>> call=ordenProduccionApi.getAllStatus();

        call.enqueue(new Callback<List<StatusPO>>() {
            @Override
            public void onResponse(Call<List<StatusPO>> call, Response<List<StatusPO>> response) {
                List<StatusPO> lista=response.body();

                for(StatusPO s:lista)
                    lstEstados.add(s);

                cargarSpinnerEstados();
            }

            @Override
            public void onFailure(Call<List<StatusPO>> call, Throwable t) {

            }
        });
    }

    private void cargarSpinnerEstados() {
        ArrayAdapter<CharSequence> adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,lstEstados);

        spEstados.setAdapter(adapter);

        spEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                estadoSelec=lstEstados.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void inicializarFechas() {
        dateA=Calendar.getInstance().getTime();
        dateB=Calendar.getInstance().getTime();

        lblFecOpDesde.setText(formatearFecha(dateA));
        lblFecOpHasta.setText(formatearFecha(dateB));
    }

    public void actionFechaHasta(View view) {
        Intent winFecha=new Intent(this,CalendarioActivity.class);
        startActivityForResult(winFecha,2);
    }

    public void actionFechaDesde(View view) {
        Intent winFecha=new Intent(this,CalendarioActivity.class);
        startActivityForResult(winFecha,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int año=data.getExtras().getInt("año");
        int mes=data.getExtras().getInt("mes");
        int dia=data.getExtras().getInt("dia");
        Calendar calendar=Calendar.getInstance();
        calendar.set(año,mes,dia);

        if(requestCode==1){
            dateA=calendar.getTime();
            lblFecOpDesde.setText(formatearFecha(dateA));
        }else{
            dateB=calendar.getTime();
            lblFecOpHasta.setText(formatearFecha(dateB));
        }
    }

    private String formatearFecha(Date f){
        Date fecha=(Date)f.clone();
        SimpleDateFormat formateador=new SimpleDateFormat("dd-MM-yyyy");
        String salida=formateador.format(fecha.getTime());
        return salida;
    }

    private OrdenProduccionApi getOrdenApiConFecha(){
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");
        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(OrdenProduccionApi.class);
    }

    private ClienteApi getClienteApiConFecha(){
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");
        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ClienteApi.class);
    }

    private Callback obtenerCallback(){
        Callback<List<OrdenProduccion>> resultado= new Callback<List<OrdenProduccion>>() {
            @Override
            public void onResponse(Call<List<OrdenProduccion>> call, Response<List<OrdenProduccion>> response) {

               listaOrdenes.clear();
                if(response.code()==200){
                    List<OrdenProduccion> lista=response.body();
                    for(OrdenProduccion op:lista)
                        listaOrdenes.add(op);

                    obtenerListadoCantReal();
                    cargarRecycler();
                }
            }

            @Override
            public void onFailure(Call<List<OrdenProduccion>> call, Throwable t) {

            }
        };
        return resultado;
    }

    public void actionCargarOrdenes(View view) {
        limpiarListas();

        DatesTransfer dt=new DatesTransfer(dateA,dateB);

        OrdenProduccionApi ordenProduccionApi=getOrdenApiConFecha();
        ClienteApi clienteApi=getClienteApiConFecha();
        Call<List<OrdenProduccion>>call=null;

        if(idCliente==0) {

            if (estadoSelec.getId() == 1L)
                call = ordenProduccionApi.getAllPOProductionBetween(dt);
            if (estadoSelec.getId() == 2L)
                call = ordenProduccionApi.getAllPOFinishedBetween(dt);
            if (estadoSelec.getId() == 3L)
                call = ordenProduccionApi.getAllPOCanceledBetween(dt);
            if (estadoSelec.getId() == 4L)
                call = ordenProduccionApi.getAllPODeliveredBetween(dt);

        }else{

            if (estadoSelec.getId() == 1L)
                call=clienteApi.getAllPOProductionBetween(idCliente,dt);
            if (estadoSelec.getId() == 2L)
                call=clienteApi.getAllPOFinishedBetween(idCliente,dt);
            if (estadoSelec.getId() == 3L)
                call=clienteApi.getAllPOCanceledBetween(idCliente,dt);
            if (estadoSelec.getId() == 4L)
                call=clienteApi.getAllPODeliveredBetween(idCliente,dt);
        }

        call.enqueue(obtenerCallback());
    }

    private void limpiarListas() {
        listaOrdenes.clear();
        listaCantidadReal.clear();

    }


    private void cargarRecycler() {
        AdapterDatosOrdenesP adapter=new AdapterDatosOrdenesP(listaOrdenes);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: Orden Nº"+
                                listaOrdenes.get(recycler.getChildAdapterPosition(v)).getId(),
                        Toast.LENGTH_LONG).show();
                ordenSelect=listaOrdenes.get(recycler.getChildAdapterPosition(v));
                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------
            }
        });


        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    private void obtenerListadoCantReal() {
        listaCantidadReal.clear();
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
        //-------------------------------------------
        ArrayList<Long> listIdOP=new ArrayList<>();
        for(OrdenProduccion op:listaOrdenes)
            listIdOP.add(op.getId());

        //-------------------------------------------
        Call<List<Integer>> call=ordenProduccionApi.getNumPiecesArray(listIdOP);

        call.enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                List<Integer> listaDatos=response.body();
                for(Integer i:listaDatos)
                    listaCantidadReal.add(i);

            }

            @Override
            public void onFailure(Call<List<Integer>> call, Throwable t) {

            }
        });


    }



    private Retrofit obtenerRetro() {
        return   new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void actionRegresar (View view){
        setResult(1);
        finish();
    }

    public void actionVerOrden(View view) {
        if(ordenSelect!=null){

            Intent ventana =new Intent(this, ConsultarOrdenActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("ordenProduccion",ordenSelect);
            ventana.putExtras(bundle);
            ventana.putExtra("NivelAcceso",NivelAcceso);
            ventana.putExtra("URL",URL);

            //startActivityForResult(ventana,1);
            startActivity(ventana);
            ordenSelect=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar uno",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void llamarGrafica(View view){
        if(listaOrdenes.size()!=0){
            Intent ventana=new Intent(this,GraficoOrdenesActivity.class);
            ventana.putExtra("lista",listaOrdenes);
            ventana.putExtra("URL",URL);
            ventana.putExtra("listaCantReal",listaCantidadReal);
            startActivity(ventana);
        }else{
            Toast.makeText(this, "Nada que mostrar", Toast.LENGTH_SHORT).show();
        }
    }
}//end
