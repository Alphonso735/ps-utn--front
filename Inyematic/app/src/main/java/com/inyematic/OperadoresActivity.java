package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosOperadores;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Modelo.Operador;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OperadoresActivity extends AppCompatActivity {
    //----------------------
    private String URL;
    //----------------------
    RecyclerView recycler;
    //---------------
    ArrayList<Operador> listaOperadores=new ArrayList<>();
    Operador operadorSelect;
    //-------------------------
    TextView selectPosRecy=null;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operadores);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        //----------
        this.getAllOperadores();

    }

    public void getAllOperadores(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OperadorApi operadorApi= retrofit.create(OperadorApi.class);
        //------------------------------

        Call<List<Operador>> call=operadorApi.getAllOperators();


        call.enqueue(new Callback<List<Operador>>() {
            @Override
            public void onResponse(Call<List<Operador>> call, Response<List<Operador>> response) {
                List<Operador> lstOperadores=response.body();

                for(Operador o:lstOperadores){
                    listaOperadores.add(o);
                }

                cargarListado();
            }

            @Override
            public void onFailure(Call<List<Operador>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Fallo en la conexión "+t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

    }
    private void cargarListado(){
        AdapterDatosOperadores adapter=new AdapterDatosOperadores(this.listaOperadores);
        //------------------------------
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+listaOperadores.get(recycler.getChildAdapterPosition(v)).getName(),
                        Toast.LENGTH_SHORT).show();
                operadorSelect=listaOperadores.get(recycler.getChildAdapterPosition(v));

                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------

            }
        });
        //-----------------------------
        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }
    public void actionAgregar(View view) {

        Intent winAgregar=new Intent(this,AgOperActivity.class);
        winAgregar.putExtra("URL",URL);

        startActivityForResult(winAgregar,1);

    }

    public void actionBorrar(View view) {

         if(operadorSelect!=null){
                Intent winBorrar=new Intent(this,BorrOperActivity.class);
                //--------------------
                Bundle bundle=new Bundle();
                bundle.putSerializable("operador",operadorSelect);
                winBorrar.putExtras(bundle);
                winBorrar.putExtra("URL",URL);
                startActivityForResult(winBorrar,1);
                operadorSelect=null;
         }else{
           Toast.makeText(getApplicationContext(),
                   "Debe seleccionar uno",
                   Toast.LENGTH_SHORT).show();
       }

    }

    public void actionModificar(View view) {
       if(operadorSelect!=null){

            Intent winModificar=new Intent(this,ModOperActivity.class);
            //-----------------
            Bundle bundle=new Bundle();
            bundle.putSerializable("operador",operadorSelect);

            winModificar.putExtras(bundle);
            winModificar.putExtra("URL",URL);

            startActivityForResult(winModificar,1);
            operadorSelect=null;
       }else{
           Toast.makeText(getApplicationContext(),
                   "Debe seleccionar uno",
                   Toast.LENGTH_SHORT).show();
       }

    }

    public void actionRecargarOperadores() {
        listaOperadores.clear();
        this.getAllOperadores();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actionRecargarOperadores();
    }

    public void actionRegresar(View view) {finish();
    }
}//end
