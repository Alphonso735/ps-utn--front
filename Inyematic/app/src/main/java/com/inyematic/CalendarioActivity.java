package com.inyematic;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;

public class CalendarioActivity extends AppCompatActivity {
    CalendarView calendario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);
        //---------------------------
        calendario=(CalendarView)findViewById(R.id.calendario);
        //--------------------------
      calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
          @Override
          public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

              Intent volver=new Intent(CalendarioActivity.this,AgOperActivity.class);
              volver.putExtra("año",year);
              volver.putExtra("mes",month);
              volver.putExtra("dia",dayOfMonth);

              setResult(1,volver);

              finish();

          }
      });
    }
}//end
