package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Modelo.Material;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModMatActivity extends AppCompatActivity {
    Material material;
    //--------------------
    TextView txtNombre,txtCantidad;
    //----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_mat);
        //-----------------------------
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        txtCantidad=(TextView)findViewById(R.id.txtCantidad);
        //-------------------------------
        cargarMaterial();
        cargarCampos();
    }
    private void cargarCampos() {
        txtNombre.setText(material.getName());
        txtCantidad.setText(String.valueOf(material.getNumOfKil()));

    }

    private void cargarMaterial() {
        Bundle bundle=getIntent().getExtras();
        material=(Material) bundle.getSerializable("material");
        URL=getIntent().getStringExtra("URL");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardar(View view) {
        Material mat=new Material();
        mat.setId(material.getId());
        mat.setName(txtNombre.getText().toString());
        mat.setNumOfKil(Float.valueOf(txtCantidad.getText().toString()));
        //--------------------------
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MaterialApi materialApi=retrofit.create(MaterialApi.class);
        //---------------------------
        Call<ResponseBody> call=materialApi.updateMaterial(mat);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:Toast.makeText(getApplicationContext(),"Se guardo el material",
                            Toast.LENGTH_LONG).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se guardo el material",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "No se guardaron los cambios",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}//end
