package com.inyematic;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosBolsasMaterial;
import com.inyematic.Interfaz.EntradaMaterialApi;
import com.inyematic.Modelo.EntradaMaterial;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BolsasMaterialActivity extends AppCompatActivity {
    //--------------------
    private String URL;
    //----------------------
    ArrayList<EntradaMaterial> listaEntrada=new ArrayList<>();
    //--------------------------
    RecyclerView recycler;
    //---------------------------
    int numPagina=0;
    int cantOrdenesAmostrar=6;
    boolean masPaginas=false;
    //---------------------------
    Button btnAnterior,btnSiguiente;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bolsas_material);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        btnAnterior=(Button)findViewById(R.id.btnAntMat);
        btnSiguiente=(Button)findViewById(R.id.btnSigMat);
        //------------------------
        obtenerEntradaMaterial();

    }

    private void obtenerEntradaMaterial() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EntradaMaterialApi entradaMaterialApi=retrofit.create(EntradaMaterialApi.class);
        //---------------------------------
        //Call<List<EntradaMaterial>> call=entradaMaterialApi.getAllMaterialInput();
        Call<List<EntradaMaterial>> call=entradaMaterialApi.getMaterialInput(numPagina,cantOrdenesAmostrar);

        call.enqueue(new Callback<List<EntradaMaterial>>() {
            @Override
            public void onResponse(Call<List<EntradaMaterial>> call, Response<List<EntradaMaterial>> response) {
                if(response.isSuccessful()){
                    listaEntrada.clear();
                    List<EntradaMaterial> lista=response.body();
                    for(EntradaMaterial em:lista){
                        listaEntrada.add(em);
                    }
                    validarMasPaginas();
                    cargarRecycler();

                }else{
                    Toast.makeText(getApplicationContext(),
                            "No se pudo obtener la lista",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<EntradaMaterial>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error de conexión",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosBolsasMaterial adapter=new AdapterDatosBolsasMaterial(listaEntrada);
        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();

    }

    public void actionAgregar(View view) {

        Intent winAgregar=new Intent(this,AgBolsaActivity.class);
        winAgregar.putExtra("URL",URL);

        startActivityForResult(winAgregar,1);
    }

    public void actionRegresar(View view) {finish();
    }

    public void actionRecargarBolsas() {
        listaEntrada.clear();
        obtenerEntradaMaterial();
    }

    private void validarMasPaginas() {
        if(listaEntrada.size()==cantOrdenesAmostrar)
            masPaginas=true;
        else
            masPaginas=false;

        validarBtnNavOrdenes();
    }
    private void validarBtnNavOrdenes() {

        if(numPagina==0)
            btnAnterior.setEnabled(false);
        else
            btnAnterior.setEnabled(true);

        if(masPaginas)
            btnSiguiente.setEnabled(true);
        else
            btnSiguiente.setEnabled(false);

    }
    public void actionSiguiente(View view) {
        if(masPaginas){
            numPagina++;
            obtenerEntradaMaterial();

        }
    }
    public void actionAnterior(View view) {
        if(numPagina>0){
            numPagina--;
            obtenerEntradaMaterial();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actionRecargarBolsas();
    }

}//end
