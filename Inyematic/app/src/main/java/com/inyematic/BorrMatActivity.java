package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Modelo.Material;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrMatActivity extends AppCompatActivity {
    Material material;
    //-----------------
    TextView lblNombre;
    //---------------------
    private String URL;
    //---------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_mat);
        //--------------------
        lblNombre=(TextView)findViewById(R.id.lblNombre);
        //--------------------
        URL=getIntent().getStringExtra("URL");
        //------------------------------------
        obtenerMaterial();
        cargarMaterial();
    }

    private void cargarMaterial() {
        lblNombre.setText(material.getName());
    }

    private void obtenerMaterial() {
        Bundle bundle=getIntent().getExtras();
        material=(Material) bundle.getSerializable("material");


    }

    public void actionBorrar(View view) {
        //------------------------------
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MaterialApi materialApi=retrofit.create(MaterialApi.class);
        //------------------------------
        Call<ResponseBody> call=materialApi.deleteMaterial(material.getId());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:Toast.makeText(getApplicationContext(),"Se borro el material",
                            Toast.LENGTH_LONG).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se puede borrar. Moldes existentes",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }
}//end
