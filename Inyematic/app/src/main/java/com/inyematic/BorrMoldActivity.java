package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MouldApi;
import com.inyematic.Modelo.Molde;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrMoldActivity extends AppCompatActivity {
    Molde molde;
    //------------------
    Button aceptar;
    Button regresar;
    TextView moldeNombre;
    //-----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_mold);
        //----------
        aceptar=(Button)findViewById(R.id.btnAceptarBorrado);
        regresar=(Button)findViewById(R.id.btnBorrar);
        moldeNombre=(TextView)findViewById(R.id.txtNombreMolde);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------

        getMolde();
        //-------------
        moldeNombre.setText(molde.getName());
    }

    private void getMolde(){
        Bundle mibundle=this.getIntent().getExtras();
        molde=(Molde)mibundle.getSerializable("molde");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void aceptarBorrado(View view) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MouldApi mouldApi=retrofit.create(MouldApi.class);

        //-----------------
        long id=molde.getId();

        Call<ResponseBody> call=mouldApi.deleteMould(id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:Toast.makeText(getApplicationContext(),"Se borro el molde",
                            Toast.LENGTH_LONG).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se puede borrar",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error conexión",Toast.LENGTH_LONG).show();
            }
        });

    }

}//end
