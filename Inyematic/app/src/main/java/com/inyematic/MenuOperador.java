package com.inyematic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuOperador extends AppCompatActivity {
    private int NivelAcceso;
    private String URL;
    //--------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_operador);
        //-------------------------------
        obtenerDatosIntent();
    }

    private void obtenerDatosIntent() {
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",0);
    }

    public void llamarWinOrdenesP(View view) {
        Intent ventana=new Intent(this,OrdenesPActivity.class);
        ventana.putExtra("URL",URL);
        ventana.putExtra("NivelAcceso",NivelAcceso);
        startActivity(ventana);
    }

    public void CerrarMenuOperador(View view){
        finish();
    }
}//end
