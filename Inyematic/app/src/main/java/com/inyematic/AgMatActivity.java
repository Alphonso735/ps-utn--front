package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Modelo.Material;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgMatActivity extends AppCompatActivity {

    EditText txtNombre;
    //----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_mat);
        //--------------------------
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        //-------------------------
        obtenerDatosIntent();
    }

    private void obtenerDatosIntent() {
        URL=getIntent().getStringExtra("URL");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardar(View view) {
        if(validar()) {


            Material material = new Material();
            material.setName(txtNombre.getText().toString());
            material.setNumOfKil(0f);
            //----------------------------------
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            MaterialApi materialApi = retrofit.create(MaterialApi.class);
            //--------------------------------------
            Call<ResponseBody> call = materialApi.saveMaterial(material);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    switch (response.code()) {
                        case 200:
                            Toast.makeText(getApplicationContext(), "Se guardo el material",
                                    Toast.LENGTH_LONG).show();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(), "No se guardo el material",
                                    Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),
                            "Error conexión",
                            Toast.LENGTH_LONG).show();
                }
            });
        }else
            Toast.makeText(this, "Controle, faltan datos.", Toast.LENGTH_SHORT).show();
    }

    private boolean validar() {
        boolean bandera=true;

        bandera=(txtNombre.getText().toString().equals(""))?false:bandera;



        return bandera;
    }
}//end
