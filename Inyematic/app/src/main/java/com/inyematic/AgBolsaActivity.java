package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.EntradaMaterialApi;
import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Modelo.EntradaMaterial;
import com.inyematic.Modelo.Material;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgBolsaActivity extends AppCompatActivity {
    //--------------------
    private String URL;
    //----------------------
    ArrayList<Material> listaMaterial=new ArrayList<>();
    Material materialSelec;
    Date fecha;
    //----------------------------
    TextView txtMaterialSelec;
    EditText txtCantBolsas;
    RadioGroup radioGroup;
    Spinner spMateriales;
    TextView txtFecha;
    //-------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_bolsa);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtMaterialSelec=(TextView)findViewById(R.id.txtMaterialSelec);
        txtCantBolsas=(EditText)findViewById(R.id.txtCantBolsas);
        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
        spMateriales=(Spinner)findViewById(R.id.spMateriales);
        txtFecha=(TextView)findViewById(R.id.txtFecha);
        //--------------------
        obtenerMateriales();
        cargarFecha();
    }

    private void cargarFecha() {
        Date fecha= Calendar.getInstance().getTime();
        this.fecha=(Date)fecha.clone();
        SimpleDateFormat formateador=new SimpleDateFormat("dd-MM-yyyy");
        txtFecha.setText(formateador.format(fecha));
    }

    private void obtenerMateriales() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MaterialApi materialApi=retrofit.create(MaterialApi.class);

        Call<List<Material>> call=materialApi.getAllMaterials();

        call.enqueue(new Callback<List<Material>>() {
            @Override
            public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                if(response.isSuccessful()){
                    List<Material> lstMaterials=response.body();

                    for(Material m:lstMaterials)
                        listaMaterial.add(m);

                    cargarSpinner();

                }else{

                    Toast.makeText(getApplicationContext(),
                            "Fallo en el Response",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<List<Material>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Fallo en la conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cargarSpinner() {
        ArrayAdapter adapter=new ArrayAdapter<Material>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                listaMaterial);
        spMateriales.setAdapter(adapter);
        //------------------------
        spMateriales.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                materialSelec= (Material) parent.getItemAtPosition(position);
                txtMaterialSelec.setText(materialSelec.getName());

                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+materialSelec.getName(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void actionGuardar(View view) {
        if(validar()){


        EntradaMaterial em=new EntradaMaterial();
        em.setMaterial(materialSelec);
        em.setNumOfBags(Integer.valueOf(txtCantBolsas.getText().toString()));
        em.setDate(fecha);
        //------------
        if(radioGroup.getCheckedRadioButtonId()==R.id.rdbtnAgregar)
            em.setStatus(true);
        else
            em.setStatus(false);
        //********************************
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");

        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        EntradaMaterialApi entradaMaterialApi=retrofit.create(EntradaMaterialApi.class);
        //---------------------------------------
        Call<ResponseBody> call;
        if(em.isStatus()){
            call=entradaMaterialApi.addMaterialInput(em.getMaterial().getId(),
                    em.getNumOfBags());
        }else{
            call=entradaMaterialApi.reduceMaterialInput(em.getMaterial().getId(),
                    em.getNumOfBags());
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se guardo correctamente",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se pudo guardar",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",Toast.LENGTH_LONG).show();
            }
        });
        } else
            Toast.makeText(this, "Controle, faltan datos.", Toast.LENGTH_SHORT).show();
    }

    private boolean validar() {
        boolean bandera=true;

        bandera=(Integer.valueOf(txtCantBolsas.getText().toString())==0)?false:bandera;



        return bandera;
    }
    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

}//end
