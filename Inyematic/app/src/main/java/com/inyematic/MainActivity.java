package com.inyematic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.inyematic.Interfaz.AppUserApi;
import com.inyematic.Interfaz.InicializarApi;
import com.inyematic.Modelo.AppUser;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    EditText txtUserName,txtPassword;
    //-----------------------------
    //private final String URL="http://192.168.0.165:8080/";
    private final String URL="http://10.42.0.1:8080/";
    private int NivelAcceso=0;

    //-----------------------------
        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //------------------------------
        txtUserName=(EditText)findViewById(R.id.txtUserName);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
        //-------------------------------
        llamarInicializar();
    }

    public void CerrarApp(View view) {
        finish();
    }

    public void Logear(View view) {
        String nombre=txtUserName.getText().toString();
        String pass=txtPassword.getText().toString();
        AppUser appUser=new AppUser();
        appUser.setUserName(nombre);
        appUser.setPassword(pass);
        //------------------------------------------
        Retrofit retrofit=obtenerRetrofit();
        AppUserApi appUserApi=retrofit.create(AppUserApi.class);
        Call<AppUser> call=appUserApi.existAppUser(appUser);

       call.enqueue(new Callback<AppUser>() {
           @Override
           public void onResponse(Call<AppUser> call, Response<AppUser> response) {

               switch(response.code()){
                   case 200:
                       Toast.makeText(getApplicationContext(),"Login Correcto",Toast.LENGTH_SHORT).show();
                       AppUser au=response.body();
                       NivelAcceso=(int)au.getEntryLevel().getId();
                       llamarSubMenu();
                       break;
                   case 404:
                       Toast.makeText(getApplicationContext(),"Login Incorrecto",Toast.LENGTH_SHORT).show();
               }

           }

           @Override
           public void onFailure(Call<AppUser> call, Throwable t) {
                   Toast.makeText(getApplicationContext(),"No se pudo conectar",Toast.LENGTH_SHORT).show();

           }
       });

    }

    private void llamarInicializar() {
        Retrofit retrofit=obtenerRetrofit();
        InicializarApi inicializarApi=retrofit.create(InicializarApi.class);
        Call<ResponseBody> call=inicializarApi.inicializar();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void llamarSubMenu() {
      Intent ventana;

      if(NivelAcceso == 1){
          ventana=new Intent(this,MenuJefe.class);
          }else if (NivelAcceso == 2){
          ventana = new Intent(this,MenuEncargado.class);
          }else{
          ventana=new Intent(this,MenuOperador.class);
      }

      ventana.putExtra("NivelAcceso",NivelAcceso);
      ventana.putExtra("URL",URL);



      startActivityForResult(ventana,1);
    }

    private Retrofit obtenerRetrofit(){
        return   new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}//end
