package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Modelo.Cliente;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModClienActivity extends AppCompatActivity {

    TextView txtNombre,txtAddress,txtPhone;
    //-----------------------------------
    Cliente cliente;
    //--------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_clien);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtAddress=(TextView)findViewById(R.id.txtAddress);
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        txtPhone=(TextView)findViewById(R.id.txtPhone);
        //----------------------
        obtenerCliente();
        cargarCampos();
    }

    private void cargarCampos() {
        txtNombre.setText(cliente.getName());
        txtAddress.setText(cliente.getAddress());
        txtPhone.setText(String.valueOf(cliente.getTelephone()));
    }

    private void obtenerCliente() {
        cliente=(Cliente) getIntent().getExtras().getSerializable("cliente");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardar(View view) {
        Cliente c=new Cliente();
        c.setId(cliente.getId());
        c.setName(txtNombre.getText().toString());
        c.setAddress(txtAddress.getText().toString());
        c.setTelephone(Long.parseLong(txtPhone.getText().toString()));
        //-------------------------
        Retrofit retrofit=obtenerRetro();

        ClienteApi clienteApi=retrofit.create(ClienteApi.class);

        Call<ResponseBody> call=clienteApi.updateClient(c);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se modifico correctamente",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se modifico correctamente",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error de conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Retrofit obtenerRetro() {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

}//end
