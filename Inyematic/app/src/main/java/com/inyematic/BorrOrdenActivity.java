package com.inyematic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.OrdenProduccion;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrOrdenActivity extends AppCompatActivity {
    //----------------------
    private String URL;
    private int NivelAcceso;
    //----------------------
    OrdenProduccion orden=null;
    //------------------------
    TextView lblNombreO;
    //------------------------
    boolean borrado=false;
    //------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_orden);
        //---------------------------------
        lblNombreO=(TextView)findViewById(R.id.lblNombreO);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",0);
        //---------------------------------
        obtenerOrden();
    }

    private void obtenerOrden() {
        orden=(OrdenProduccion)getIntent().getExtras().getSerializable("orden");
        lblNombreO.setText("Nº "+orden.getId());
    }

    public void actionRegresar(View view) {/*
        if(borrado){
            Intent ventana=new Intent(this,OrdenesPActivity.class);
            ventana.putExtra("URL",URL);
            ventana.putExtra("NivelAcceso",NivelAcceso);
            startActivity(ventana);
        finish();
        }else{
            finish();
        }*/
        if(borrado)
            setResult(2);
        else
            setResult(3);
        finish();
    }

    public void actionBorrar(View view) {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<ResponseBody>call =ordenProduccionApi.deleteProductionOrder(orden.getId());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200://ok
                        borrado=true;
                        Toast.makeText(getApplicationContext(),"Se borro la orden",
                                Toast.LENGTH_LONG).show();
                        break;
                    case 400://bad request
                        Toast.makeText(getApplicationContext(),"Error en los datos",
                                Toast.LENGTH_LONG).show();
                        break;

                    case 404://not found
                        Toast.makeText(getApplicationContext(),"No se puede borrar",
                            Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error de conexión",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private Retrofit obtenerRetro() {
        return   new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}//end
