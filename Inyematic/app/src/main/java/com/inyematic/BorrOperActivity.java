package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Modelo.Operador;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrOperActivity extends AppCompatActivity {
    Operador operador;
    //----------------------
    private String URL;
    //----------------------
    TextView txtNombre;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_oper);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtNombre=(TextView)findViewById(R.id.txtNomOper);
        //-------------------
        obtenerOperador();
        //------------------
        txtNombre.setText(operador.getName());
    }

    private void obtenerOperador() {
        Bundle bundle=getIntent().getExtras();
        operador=(Operador)bundle.getSerializable("operador");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionBorrar(View view) {
        //-----------------------------------------
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");

        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        OperadorApi operadorApi=retrofit.create(OperadorApi.class);
        //---------------------------------------------
        long id=operador.getId();

       Call<ResponseBody> call=operadorApi.deleteOperator(id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:Toast.makeText(getApplicationContext(),"Se borro el Operador",
                            Toast.LENGTH_LONG).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se puede borrar",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error de conexión",Toast.LENGTH_LONG).show();
            }
        });

    }

}//end
