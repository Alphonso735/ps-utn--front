package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Modelo.Cliente;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrClienActivity extends AppCompatActivity {
    //--------------------
    private String URL;
    //----------------------
    Cliente cliente;
    //-------------------------------------------
    TextView lblNombre;
    //-------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_clien);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        lblNombre=(TextView)findViewById(R.id.lblNombreC);
        //-------------------------------
        obtenerCliente();
        lblNombre.setText(cliente.getName());
    }

    private void obtenerCliente() {
        Bundle bundle=getIntent().getExtras();
        cliente=(Cliente)bundle.getSerializable("cliente");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionBorrar(View view) {
        Retrofit retrofit=obtenerRetro();
        ClienteApi clienteApi=retrofit.create(ClienteApi.class);

        Call<ResponseBody> call=clienteApi.deleteClient(this.cliente.getId());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se borro correctamente",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se pudo borrar",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error de conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Retrofit obtenerRetro() {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

}//end
