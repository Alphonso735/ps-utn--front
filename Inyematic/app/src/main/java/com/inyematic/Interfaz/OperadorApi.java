package com.inyematic.Interfaz;

import com.inyematic.Modelo.Operador;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OperadorApi {
    @GET("Operators")
    Call<List<Operador>> getAllOperators();

    @POST("Operators")
    Call<ResponseBody> saveOperator(@Body Operador operador);

    @DELETE("Operators/{idOperator}")
    Call<ResponseBody> deleteOperator(@Path("idOperator")long id);

    @PUT("Operators")
    Call<ResponseBody> updateOperador(@Body Operador operador);
}
