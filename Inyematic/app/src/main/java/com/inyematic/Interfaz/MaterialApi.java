package com.inyematic.Interfaz;


import com.inyematic.Modelo.Material;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MaterialApi {
    @GET("Materials")
    Call<List<Material>> getAllMaterials();

    @POST("Materials")
    Call<ResponseBody> saveMaterial(@Body Material material);

    @PUT("Materials")
    Call<ResponseBody> updateMaterial(@Body Material material);

    @DELETE("Materials/{idMaterial}")
    Call<ResponseBody> deleteMaterial(@Path("idMaterial") long idMaterial);
}
