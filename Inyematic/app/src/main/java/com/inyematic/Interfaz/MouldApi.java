package com.inyematic.Interfaz;

import com.inyematic.Modelo.Molde;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MouldApi {

    @GET("Moulds")
    Call<List<Molde>> getAllMoulds();

    @GET("Moulds/{page}/{mount}")
    Call<List<Molde>> getMoulds(@Path("page")int page,@Path("mount")int mount);

    @PUT("Moulds")
    Call<ResponseBody>updateMould(@Body Molde molde);

    @POST("Moulds")
    Call<ResponseBody>saveMould(@Body Molde molde);

    @DELETE("Moulds/{idMould}")
    Call<ResponseBody>deleteMould(@Path("idMould")long idMould);
}
