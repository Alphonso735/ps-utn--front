package com.inyematic.Interfaz;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface InicializarApi {
    @GET("Inicializar")
    Call<ResponseBody> inicializar();
}
