package com.inyematic.Interfaz;

import com.inyematic.Modelo.AppUser;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AppUserApi {

    @POST("AppUsers")
    Call<AppUser> existAppUser(@Body AppUser appUser);

    @GET("AppUsers")
    Call<List<AppUser>> getAllAppUser();

    @POST("AppUsers/add")
    Call<ResponseBody> saveAppUser(@Body AppUser au);

    @PUT("AppUsers")
    Call<ResponseBody> updateAppUser(@Body AppUser nuevo);

    @DELETE("AppUser/{idAU}")
    Call<ResponseBody> deleteAppUser(@Path("idAU") long id);
}
