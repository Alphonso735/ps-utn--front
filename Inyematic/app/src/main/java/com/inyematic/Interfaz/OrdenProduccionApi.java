package com.inyematic.Interfaz;

import com.inyematic.Modelo.Caja;
import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.Modelo.StatusPO;
import com.inyematic.Modelo.dto.DatesTransfer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrdenProduccionApi {

    @POST("ProductionOrders")
    Call<ResponseBody> saveProductionOrder(@Body OrdenProduccion ordenProduccion);
//---------------------------------------------------------------------------
   @GET("ProductionOrders/production/{page}/{mount}")
   Call<List<OrdenProduccion>> getPOproduction(@Path("page")int page,@Path("mount")int mount);

    @GET("ProductionOrders/canceled/{page}/{mount}")
    Call<List<OrdenProduccion>> getPOcanceled(@Path("page")int page,@Path("mount")int mount);

    @GET("ProductionOrders/finished/{page}/{mount}")
    Call<List<OrdenProduccion>> getPOfinished(@Path("page")int page,@Path("mount")int mount);

    @GET("ProductionOrders/delivered/{page}/{mount}")
    Call<List<OrdenProduccion>> getPOdelivered(@Path("page")int page,@Path("mount")int mount);
//------------------------------------------------------------------
    @GET("ProductionOrders/status")
    Call<List<StatusPO>> getAllStatus();

    @PUT("ProductionOrders")
    Call<ResponseBody> updateProductionOrder(@Body OrdenProduccion ordenProduccion);
    //---------------------------------------------------------------------------
    @GET("ProductionOrders/{idOP}/DeliveryBox")
    Call<List<Caja>> getAllDeliveryBoxFrom(@Path("idOP") long id);

    @POST("ProductionOrders/{idOP}/DeliveryBox")
    Call<ResponseBody> saveBoxToProductionOrder(@Body Caja caja, @Path("idOP") long id);

    @DELETE("ProductionOrders/{idOP}/DeliveryBox/{idBox}")
    Call<ResponseBody> deleteDeliveryBoxFrom(@Path("idOP") long idOP,@Path("idBox") long idBox);
    //---------------------------------------------------------------------------
    @DELETE("ProductionOrders/{idOP}")
    Call<ResponseBody> deleteProductionOrder(@Path("idOP") long id);

    //---------------------------------------------------------------------------
    @POST("ProductionOrders/production/between")
    Call<List<OrdenProduccion>> getAllPOProductionBetween(@Body DatesTransfer datesTransfer);

    @POST("ProductionOrders/finished/between")
    Call<List<OrdenProduccion>> getAllPOFinishedBetween(@Body DatesTransfer dt);

    @POST("ProductionOrders/canceled/between")
    Call<List<OrdenProduccion>> getAllPOCanceledBetween(@Body DatesTransfer dt);

    @POST("ProductionOrders/delivered/between")
    Call<List<OrdenProduccion>> getAllPODeliveredBetween(@Body DatesTransfer dt);

    //----------------------------------------------------------------------------
    @GET("ProductionOrders/{idOP}/NumOfPiecesInBoxes")
    Call<ResponseBody> getNumOfPiecesInBoxes(@Path("idOP")long idOP);

    @POST("ProductionOrders/getNumPiecesArray")
    Call<List<Integer>> getNumPiecesArray(@Body ArrayList<Long> listIdOP);
}
