package com.inyematic.Interfaz;

import com.inyematic.Modelo.Cliente;
import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.Modelo.dto.DatesTransfer;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ClienteApi {

    @POST("Clients")
    Call<ResponseBody> saveClient(@Body Cliente cliente);

    @GET("Clients")
    Call<List<Cliente>> getAllClients();

    @GET("Clients/{page}/{mount}")
    Call<List<Cliente>> getClients(@Path("page")int page,@Path("mount")int mount);

    @PUT("Clients")
    Call<ResponseBody> updateClient(@Body Cliente cliente);

    @DELETE("Clients/{idClient}")
    Call<ResponseBody> deleteClient(@Path("idClient") long idClient);
    //-----------------------------------------------------------------------
    @POST("Clients/{idClient}/production/between")
    Call<List<OrdenProduccion>> getAllPOProductionBetween(@Path("idClient") long id,@Body DatesTransfer dt);

    @POST("Clients/{idClient}/finished/between")
    Call<List<OrdenProduccion>> getAllPOFinishedBetween(@Path("idClient") long id,@Body DatesTransfer dt);

    @POST("Clients/{idClient}/canceled/between")
    Call<List<OrdenProduccion>> getAllPOCanceledBetween(@Path("idClient") long id,@Body DatesTransfer dt);

    @POST("Clients/{idClient}/delivered/between")
    Call<List<OrdenProduccion>> getAllPODeliveredBetween(@Path("idClient") long id,@Body DatesTransfer dt);
}
