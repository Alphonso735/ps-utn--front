package com.inyematic.Interfaz;

import com.inyematic.Modelo.EntradaMaterial;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EntradaMaterialApi {

    @GET("MaterialInput")
    Call<List<EntradaMaterial>> getAllMaterialInput();

    @GET("MaterialInput/{page}/{mount}")
    Call<List<EntradaMaterial>> getMaterialInput(@Path("page")int page,@Path("mount")int mount);

    @POST("MaterialInput/Add/{idMaterial}/{numBags}")
    Call<ResponseBody> addMaterialInput(@Path("idMaterial")long idMaterial,@Path("numBags")int numBags);

    @POST("MaterialInput/Reduce/{idMaterial}/{numBags}")
    Call<ResponseBody> reduceMaterialInput(@Path("idMaterial")long idMaterial,@Path("numBags")int numBags);
}
