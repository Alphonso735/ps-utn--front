package com.inyematic.Interfaz;

import com.inyematic.Modelo.EntryLevel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EntryLevelApi {
    @GET("EntryLevels")
    Call<List<EntryLevel>> getAllEntryLevel();
}
