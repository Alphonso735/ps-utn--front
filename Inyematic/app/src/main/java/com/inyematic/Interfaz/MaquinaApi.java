package com.inyematic.Interfaz;

import com.inyematic.Modelo.Maquina;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MaquinaApi {

    @GET("Machines")
    Call<List<Maquina>> getAllMachines();

    @POST("Machines")
    Call<ResponseBody> saveMachine(@Body Maquina maquina);

    @PUT("Machines")
    Call<ResponseBody> updateMachine(@Body Maquina maquina);

    @DELETE("Machines/{idMachine}")
    Call<ResponseBody> deleteMachine(@Path("idMachine")long idMachine);
}
