package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosMoldes;
import com.inyematic.Interfaz.MouldApi;
import com.inyematic.Modelo.Molde;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoldesActivity extends AppCompatActivity {

    ArrayList<Molde> listaDatos=new ArrayList<Molde>();
    Molde moldeSelected=null;
    //----------------------
    RecyclerView recycler;
    //----------------------
    private String URL;
    //-------------------------
    TextView selectPosRecy=null;
    //---------------------------
    int numPagina=0;
    int cantOrdenesAmostrar=5;
    boolean masPaginas=false;
    //---------------------------
    Button btnAnterior,btnSiguiente;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moldes);
        //---------------------------------
        recycler=(RecyclerView) findViewById(R.id.lista);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        btnAnterior=(Button)findViewById(R.id.btnAntMold);
        btnSiguiente=(Button)findViewById(R.id.btnSigMold);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        this.getMoulds();
    }

    private void selectionHandler() {
        //**************************Aca manejo la selección de los moldes****************
        AdapterDatosMoldes adaptador=new AdapterDatosMoldes(listaDatos);

        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Seleccion: "+listaDatos.get(recycler.getChildAdapterPosition(v))
                                .getName(),Toast.LENGTH_SHORT).show();
                //aca cargo en moldeSelected el objeto del molde seleccionado.
                moldeSelected=listaDatos.get(recycler.getChildAdapterPosition(v));

                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------
            }
        });

        recycler.setAdapter(adaptador);

        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    public void actionModificar(View view){
        if(moldeSelected!=null){
            //*********Aca llamo a la Activity de modificar********************
            Intent winModificar=new Intent(MoldesActivity.this,ModMoldActivity.class);
            //---------------
            Bundle mibundle=new Bundle();
            mibundle.putSerializable("molde",this.moldeSelected);

            winModificar.putExtras(mibundle);
            winModificar.putExtra("URL",URL);
            //------------------
            startActivityForResult(winModificar,1);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Seleccione uno antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void getMoulds(){
        //**************Aca traigo los moldes******************************
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MouldApi mouldApi= retrofit.create(MouldApi.class);

        Call<List<Molde>> call=mouldApi.getMoulds(numPagina,cantOrdenesAmostrar);

        call.enqueue(new Callback<List<Molde>>() {
            @Override
            public void onResponse(Call<List<Molde>> call, Response<List<Molde>> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"Error en obtener los datos",Toast.LENGTH_SHORT).show();
                    return;
                }
                //*******
                listaDatos.clear();
                List<Molde> lstMolde=response.body();   //con esto traigo todo del get

                //cargo al listaDatos
                for(int i=0;i<lstMolde.size();i++) {
                    listaDatos.add(lstMolde.get(i));
                }

                validarMasPaginas();

                //********
                selectionHandler();

            }

            @Override
            public void onFailure(Call<List<Molde>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error en la conexión",Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void actionAgregar(View view){

        //********Aca llamo a la Activity de agregar
        Intent winAgregar=new Intent(this,AgMoldActivity.class);
        winAgregar.putExtra("URL",URL);
        startActivityForResult(winAgregar,1);

    }

    public void recargarMoldes() {
        listaDatos.clear();
        this.getMoulds();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        recargarMoldes();
    }

    public void actionBorrar(View view) {
        if(moldeSelected!=null){
            Intent winBorrar=new Intent(this,BorrMoldActivity.class);

            Bundle mibundle=new Bundle();
            mibundle.putSerializable("molde",this.moldeSelected);

            winBorrar.putExtras(mibundle);
            winBorrar.putExtra("URL",URL);
            startActivityForResult(winBorrar,1);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionRegresar(View view) {finish();
    }

    private void validarMasPaginas() {
        if(listaDatos.size()==cantOrdenesAmostrar)
            masPaginas=true;
        else
            masPaginas=false;

        validarBtnNavOrdenes();
    }
    private void validarBtnNavOrdenes() {

        if(numPagina==0)
            btnAnterior.setEnabled(false);
        else
            btnAnterior.setEnabled(true);

        if(masPaginas)
            btnSiguiente.setEnabled(true);
        else
            btnSiguiente.setEnabled(false);

    }
    public void actionSiguiente(View view) {
        if(masPaginas){
            numPagina++;
            getMoulds();

        }
    }
    public void actionAnterior(View view) {
        if(numPagina>0){
            numPagina--;
            getMoulds();
        }
    }
}//end
