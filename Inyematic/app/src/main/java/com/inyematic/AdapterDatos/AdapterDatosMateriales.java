package com.inyematic.AdapterDatos;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inyematic.Modelo.Material;
import com.inyematic.R;

import java.util.ArrayList;

public class AdapterDatosMateriales
        extends RecyclerView.Adapter<AdapterDatosMateriales.ViewHolderDatos>
        implements View.OnClickListener {

    private View.OnClickListener listener;
    ArrayList<Material> listaDatos;
    //--------------------------

    public AdapterDatosMateriales(ArrayList<Material> listaDatos) {
        this.listaDatos = listaDatos;
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista,
                null,
                false);
        view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignarDatos(listaDatos.get(i));

    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView dato;
        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            dato=(TextView)itemView.findViewById(R.id.idDato);
            //------------------------

        }

        public void asignarDatos(Material material) {
            String cadena="Material: "+material.getName()+" Cantidad: "+material.getNumOfKil();
            dato.setText(cadena);
        }
    }
}