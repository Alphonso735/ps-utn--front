package com.inyematic.AdapterDatos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.R;

import java.util.ArrayList;

public class AdapterDatosOrdenesP
        extends RecyclerView.Adapter<AdapterDatosOrdenesP.ViewHolderDatos>
        implements View.OnClickListener {

    private View.OnClickListener listener;
    ArrayList<OrdenProduccion> listaDatos;

    public AdapterDatosOrdenesP(ArrayList<OrdenProduccion> listaDatos){
        this.listaDatos=listaDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista,
                null,
                false);
        view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignarDatos(listaDatos.get(i));
    }

    @Override
    public int getItemCount() {
        return  listaDatos.size();
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView dato;
        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            dato=(TextView)itemView.findViewById(R.id.idDato);
        }

        public void asignarDatos(OrdenProduccion ordenProduccion) {/*
            String molde="Molde: "+ordenProduccion.getMould().getName();
            String cantidad="Cant: "+ordenProduccion.getPieceCount();
            String operador="Operador: "+ordenProduccion.getOperator().getName();
            String maquina="Maquina: "+ordenProduccion.getMachine().getName();

            dato.setText(molde+cantidad+operador+maquina);
            */
            String texto;
            texto="Orden Nº "+String.valueOf(ordenProduccion.getId());
            texto+=" Operador:"+ordenProduccion.getOperator().getName();
            texto+=" Molde:"+ordenProduccion.getMould().getName();
            dato.setText(texto);
        }
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

}
