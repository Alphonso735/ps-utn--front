package com.inyematic.AdapterDatos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inyematic.Modelo.Caja;
import com.inyematic.R;

import java.util.ArrayList;

public class AdapterDatosCajas
        extends RecyclerView.Adapter<AdapterDatosCajas.ViewHolderDatos>
        implements View.OnClickListener {

    private View.OnClickListener listener;

    ArrayList<Caja> listaDatos;


    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public AdapterDatosCajas(ArrayList<Caja> listaDatos) {
        this.listaDatos = listaDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista,
                null,
                false);
        view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignarDatos(listaDatos.get(i));
    }

    @Override
    public int getItemCount() {
        return  listaDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView dato;
        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            dato=(TextView)itemView.findViewById(R.id.idDato);
        }

        public void asignarDatos(Caja caja) {
            String texto="Caja Nº "+caja.getId()+" cant. piezas: "+caja.getNumOfItems();
            dato.setText(texto);
        }
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

}//end
