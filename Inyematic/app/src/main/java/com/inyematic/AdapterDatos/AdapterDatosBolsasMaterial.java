package com.inyematic.AdapterDatos;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inyematic.Modelo.EntradaMaterial;
import com.inyematic.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AdapterDatosBolsasMaterial
        extends RecyclerView.Adapter<AdapterDatosBolsasMaterial.ViewHolderDatos> {
    private View.OnClickListener listener;

    ArrayList<EntradaMaterial> listaDatos;





    public AdapterDatosBolsasMaterial(ArrayList<EntradaMaterial> listaDatos) {
        this.listaDatos = listaDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista,
                null,
                false);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignarDatos(listaDatos.get(i));
    }

    @Override
    public int getItemCount() {
        return  listaDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView dato;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            dato=(TextView)itemView.findViewById(R.id.idDato);

        }

        public void asignarDatos(EntradaMaterial em) {
            String texto;

            texto="Material: "+em.getMaterial().getName();
            texto=texto+" Cant. Bolsas: "+String.valueOf(em.getNumOfBags());

            //------------------
            SimpleDateFormat formateador=new SimpleDateFormat("dd-MM-yyyy");
            String fech=formateador.format(em.getDate());

            texto=texto+" Fecha:"+fech;

            //---------------------------
            int color;
            if(em.isStatus())
                color= Color.BLUE;
            else
                color=Color.RED;

            dato.setText(texto);
            dato.setTextColor(color);
        }
    }


}
