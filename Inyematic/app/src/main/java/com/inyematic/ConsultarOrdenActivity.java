package com.inyematic;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.Cliente;
import com.inyematic.Modelo.Maquina;
import com.inyematic.Modelo.Molde;
import com.inyematic.Modelo.Operador;
import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.Modelo.StatusPO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsultarOrdenActivity extends AppCompatActivity {

    ArrayList<Molde> lstMoldes=new ArrayList<>();
    Molde moldeSelect;
    TextView lblMolde;
    //-----------------------------------------
    ArrayList<Operador> lstOperadores=new ArrayList<>();
    Operador operadorSelect;
    Spinner spOperadores;
    TextView lblOperador;
    //------------------------------------------
    ArrayList<Maquina> lstMaquinas=new ArrayList<>();
    Maquina maquinaSelect;
    Spinner spMaquinas;
    TextView lblMaquina;
    //-------------------------------------------
    ArrayList<Cliente> lstClientes=new ArrayList<>();
    Cliente clienteSelect;
    TextView lblCliente;
    //-------------------------------------------
    EditText txtCantPiezas;
    TextView lblCantPiezasCajas;
    //------------------------------------------
    OrdenProduccion opSelected;
    TextView lblNombreOrden;
    //------------------------------------------
    RadioGroup rgEstado;
    RadioButton rbtnTerminado,rbtnProduccion,rbtnEntregado;
    TextView lblEstado;
    //------------------------------------------
    Button btnModificar,btnCancelar,btnGuardar,btnCancelarOrden;
    //------------------------------------------

    private static final Long PRODUCCION=1L;
    private static final Long TERMINADO=2L;
    private static final Long CANCELADO=3L;
    private static final Long ENTREGADO=4L;
    //------------------------------------------
    ArrayList<StatusPO> lstEstados=new ArrayList<>();
    //----------------------
    private String URL;
    private int NivelAcceso;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_orden);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",0);
        //---------------------------------

        spMaquinas=(Spinner)findViewById(R.id.spMaquina);
        spOperadores=(Spinner)findViewById(R.id.spOperador);


        txtCantPiezas=(EditText)findViewById(R.id.txtCantPiezas);
        lblCantPiezasCajas=(TextView)findViewById(R.id.lblPiezasAct);
        lblNombreOrden=(TextView)findViewById(R.id.lblNombreOrden);
        lblMolde=(TextView)findViewById(R.id.lblMolde);
        lblMaquina=(TextView)findViewById(R.id.lblMaquina);
        lblOperador=(TextView)findViewById(R.id.lblOperador);
        lblCliente=(TextView)findViewById(R.id.lblCliente);
        lblEstado=(TextView)findViewById(R.id.lblEstado);

        rgEstado=(RadioGroup)findViewById(R.id.rgEstado);
        rbtnTerminado=(RadioButton)findViewById(R.id.rbtnTerminado);
        rbtnProduccion=(RadioButton)findViewById(R.id.rbtnProduccion);
        rbtnEntregado=(RadioButton)findViewById(R.id.rbtnEntregado);

        btnModificar=(Button)findViewById(R.id.btnModificar);
        btnCancelar=(Button)findViewById(R.id.btnCancelar);
        btnGuardar=(Button)findViewById(R.id.btnGuardar);
        btnCancelarOrden=(Button)findViewById(R.id.btnCancelarOrden);
        //---------------------------------
        cargarOrdenProduccion();
        //-----------------
        cargarListEstados();
        //-----------------
        cargarMaquinas();
        cargarOperadores();
        //---------------------------

        habilitarCampos(false);
        inicializarBotones();
    }

    private void inicializarBotones() {
        btnCancelar.setEnabled(false);
        btnGuardar.setEnabled(false);
    }

    private void habilitarCampos(boolean b) {
        spMaquinas.setEnabled(b);
        spOperadores.setEnabled(b);
        txtCantPiezas.setEnabled(b);
        rbtnProduccion.setEnabled(b);
        rbtnTerminado.setEnabled(b);
        rbtnEntregado.setEnabled(b);

        long estado=opSelected.getStatus().getId();
        if(estado==PRODUCCION && b){
           rbtnEntregado.setEnabled(false);
        }
        //if(estado==TERMINADO && b){
          //  rbtnProduccion.setEnabled(false);
        //}
        if(estado==ENTREGADO && b){
            rbtnProduccion.setEnabled(false);
            rbtnTerminado.setEnabled(false);
        }
        setearCamposSegunNivelAcceso();
    }

    private void setearCamposSegunNivelAcceso() {
        if(NivelAcceso==3){
            spMaquinas.setEnabled(false);
            spOperadores.setEnabled(false);
            txtCantPiezas.setEnabled(false);
            btnCancelarOrden.setEnabled(false);
        }
    }

    private void cargarListEstados() {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<List<StatusPO>> call=ordenProduccionApi.getAllStatus();

        call.enqueue(new Callback<List<StatusPO>>() {
            @Override
            public void onResponse(Call<List<StatusPO>> call, Response<List<StatusPO>> response) {
                List<StatusPO> lista=response.body();

                for(StatusPO s:lista)
                    lstEstados.add(s);
            }

            @Override
            public void onFailure(Call<List<StatusPO>> call, Throwable t) {

            }
        });
    }

    private void cargarOrdenProduccion() {
        OrdenProduccion op=(OrdenProduccion) getIntent().getExtras().getSerializable("ordenProduccion");
        //----------------
        clienteSelect=op.getCliente();
        maquinaSelect=op.getMachine();
        moldeSelect=op.getMould();
        operadorSelect=op.getOperator();

        //----------------
        txtCantPiezas.setText(String.valueOf(op.getPieceCount()));
        obtenerCantPiezasCajas(op.getId());
        lblCliente.setText(clienteSelect.getName());
        lblMolde.setText(moldeSelect.getName());
        lblEstado.setText(op.getStatus().getName());
        //---------------------
        long estado= op.getStatus().getId();
        if (estado == PRODUCCION)
            rgEstado.check(R.id.rbtnProduccion);

        if (estado == TERMINADO)
            rgEstado.check(R.id.rbtnTerminado);

        if(estado == ENTREGADO)
            rgEstado.check(R.id.rbtnTerminado);
        //---------------------
        opSelected = op;
        lblNombreOrden.setText("Nº "+opSelected.getId());
        //-----------------

    }

    private void obtenerCantPiezasCajas(long idOP) {
        Retrofit retro=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retro.create(OrdenProduccionApi.class);
        Call<ResponseBody> call=ordenProduccionApi.getNumOfPiecesInBoxes(idOP);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ResponseBody rb=response.body();
                if(response.code()==200){
                    String cantidad="0";
                    try {
                        cantidad =new String(rb.bytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                        cantidad="0";
                    }
                    lblCantPiezasCajas.setText(cantidad);
                }else{
                    lblCantPiezasCajas.setText("0");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    private void cargarMaquinas(){
        Retrofit retrofit=this.obtenerRetro();
        MaquinaApi maquinaApi=retrofit.create(MaquinaApi.class);
        Call<List<Maquina>> call=maquinaApi.getAllMachines();

        call.enqueue(new Callback<List<Maquina>>() {
            @Override
            public void onResponse(Call<List<Maquina>> call, Response<List<Maquina>> response) {
                List<Maquina> lista=response.body();
                for(Maquina m:lista)
                    lstMaquinas.add(m);
                cargarSpMaquinas();
            }

            @Override
            public void onFailure(Call<List<Maquina>> call, Throwable t) {

            }
        });
    }

    private void cargarSpMaquinas() {
        ArrayAdapter adapter=new ArrayAdapter<Maquina>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstMaquinas);
        spMaquinas.setAdapter(adapter);
        //*********************
        int posMaquina=-1;
        for(Maquina m:lstMaquinas){
            if(m.getId()==maquinaSelect.getId())
                posMaquina=lstMaquinas.indexOf(m);
        }
            spMaquinas.setSelection(posMaquina);
        //----------------------
        spMaquinas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maquinaSelect=(Maquina)parent.getItemAtPosition(position);
                lblMaquina.setText(maquinaSelect.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarOperadores(){
        Retrofit retrofit=obtenerRetro();
        OperadorApi operadorApi=retrofit.create(OperadorApi.class);

        Call<List<Operador>> call=operadorApi.getAllOperators();

        call.enqueue(new Callback<List<Operador>>() {
            @Override
            public void onResponse(Call<List<Operador>> call, Response<List<Operador>> response) {
                List<Operador> lista=response.body();
                for(Operador o:lista)
                    lstOperadores.add(o);

                cargarSpOperadores();
            }

            @Override
            public void onFailure(Call<List<Operador>> call, Throwable t) {

            }
        });
    }

    private void cargarSpOperadores() {
        ArrayAdapter adapter=new ArrayAdapter<Operador>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstOperadores);
        spOperadores.setAdapter(adapter);

        //*********************
        int posOperador=-1;
        for(Operador o:lstOperadores){
            if(o.getId()==operadorSelect.getId())
                posOperador=lstOperadores.indexOf(o);
        }
        spOperadores.setSelection(posOperador);
        //*********************
        spOperadores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                operadorSelect=(Operador)parent.getItemAtPosition(position);
                lblOperador.setText(operadorSelect.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private Retrofit obtenerRetro(){
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");

        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return  retrofit;
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardar(View view) {
        OrdenProduccion newOP=new OrdenProduccion();
        newOP.setId(opSelected.getId());
        newOP.setCliente(clienteSelect);
        newOP.setMachine(maquinaSelect);
        newOP.setMould(moldeSelect);
        newOP.setOperator(operadorSelect);
        newOP.setPieceCount(Integer.parseInt(txtCantPiezas.getText().toString()));
        newOP.setStatus(obtenerEstado());

    //-----------------------------------------------------------
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<ResponseBody>call=ordenProduccionApi.updateProductionOrder(newOP);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int codigo=response.code();

                switch (codigo){
                    case 200://ok
                        Toast.makeText(getApplicationContext(), "Se guardo", Toast.LENGTH_SHORT).show();
                        habilitarCampos(false);
                        btnCancelar.setEnabled(false);
                        btnModificar.setEnabled(true);
                        btnGuardar.setEnabled(false);
                        break;
                    case 507://insuficiente material
                        Toast.makeText(getApplicationContext(), "Insuficiente material", Toast.LENGTH_SHORT).show();
                        break;
                    case 400://bad request
                        Toast.makeText(getApplicationContext(), "Error en los datos", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(getApplicationContext(),
                        "Error de conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private StatusPO obtenerEstado() {
        StatusPO estado=null;
        int chekeado=rgEstado.getCheckedRadioButtonId();

        if(chekeado==R.id.rbtnProduccion)
            estado=lstEstados.get(0);

        if(chekeado==R.id.rbtnTerminado)
            estado=lstEstados.get(1);

        if(chekeado==R.id.rbtnEntregado)
            estado=lstEstados.get(3);
         return estado;
    }

    public void actionModificar(View view) {
        habilitarCampos(true);
        btnCancelar.setEnabled(true);
        btnGuardar.setEnabled(true);

    }

    public void actionCancelar(View view) {
        habilitarCampos(false);
        btnGuardar.setEnabled(false);
        btnCancelar.setEnabled(false);
    }

    public void borrarOrden(View view) {
        Intent ventana=new Intent(this,BorrOrdenActivity.class);
        ventana.putExtra("URL",URL);
        ventana.putExtra("NivelAcceso",NivelAcceso);
        Bundle bundle=new Bundle();
        bundle.putSerializable("orden",opSelected);
        ventana.putExtras(bundle);

        startActivityForResult(ventana,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==2)
            finish();
    }
}//end
