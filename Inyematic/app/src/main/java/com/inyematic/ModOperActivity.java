package com.inyematic;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Modelo.Operador;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModOperActivity extends AppCompatActivity {
    //----------------------
    private String URL;
    //----------------------
    Operador operador;
    Date fecha;
    //------------------
    TextView txtNombre,txtFecha,txtAddress;
    //------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_oper);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtNombre=(TextView)this.findViewById(R.id.txtNom);
        txtFecha=(TextView)findViewById(R.id.lblDate);
        txtAddress=(TextView)findViewById(R.id.txtAddr);
        //-------------------------------
        obtenerOperador();
        cargarDatos();
    }

    private void cargarDatos() {
        txtNombre.setText(operador.getName());
        //------------
        actualizarFecha(operador.getBirthDate());
        //---------------
        txtAddress.setText(operador.getAddress());

    }

    private void actualizarFecha(@org.jetbrains.annotations.NotNull Date fecha) {
        this.fecha=(Date)fecha.clone();
        SimpleDateFormat formateador=new SimpleDateFormat("dd-MM-yyyy");
        String salida=formateador.format(fecha.getTime());
        txtFecha.setText(salida);
    }

    private void obtenerOperador() {
        Bundle bundle=getIntent().getExtras();
        operador=(Operador) bundle.getSerializable("operador");
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionAceptarCambios(View view) {
        Operador nuevoOp=new Operador();
        nuevoOp.setId(operador.getId());
        nuevoOp.setBirthDate(this.fecha);
        nuevoOp.setAddress(txtAddress.getText().toString());
        nuevoOp.setName(txtNombre.getText().toString());
        //-------------------------------------
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");

        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        OperadorApi operadorApi=retrofit.create(OperadorApi.class);
        //---------------------------------------------
        Call<ResponseBody> call=operadorApi.updateOperador(nuevoOp);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:Toast.makeText(getApplicationContext(),
                            "Se modifico el operador",
                            Toast.LENGTH_SHORT).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se puede modificar",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error de conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void actionFecha(View view) {
        Intent winFecha=new Intent(this,CalendarioActivity.class);
        startActivityForResult(winFecha,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //--------------------------

        if(resultCode == RESULT_CANCELED){
            Toast.makeText(this,"Resultado Cancelado",Toast.LENGTH_LONG).show();
        }else{
            int año=data.getExtras().getInt("año");
            int mes=data.getExtras().getInt("mes");
            int dia=data.getExtras().getInt("dia");

            Calendar calendar=Calendar.getInstance();
            calendar.set(año,mes,dia);
            this.actualizarFecha(calendar.getTime());


        }
    }

}//end
