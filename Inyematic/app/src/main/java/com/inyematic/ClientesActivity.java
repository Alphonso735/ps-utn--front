package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosClientes;
import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Modelo.Cliente;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientesActivity extends AppCompatActivity {


    ArrayList<Cliente> listaClientes=new ArrayList<>();
    Cliente clienteSelect;
    //--------------------
    RecyclerView recycler;
    //--------------------
    private String URL;
    //---------------------------
    int numPagina=0;
    int cantOrdenesAmostrar=5;
    boolean masPaginas=false;
    //---------------------------
    Button btnAnterior,btnSiguiente;
    //-------------------------
    TextView selectPosRecy=null;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        btnAnterior=(Button)findViewById(R.id.btnAntClien);
        btnSiguiente=(Button)findViewById(R.id.btnSigClien);
        //----------
        obtenerClientes();
    }

    private void obtenerClientes() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //-------------------------------------
        ClienteApi clienteApi=retrofit.create(ClienteApi.class);

        Call<List<Cliente>> call=clienteApi.getAllClients();

        call.enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                listaClientes.clear();
                List<Cliente> lista=response.body();
                for(Cliente c:lista)
                    listaClientes.add(c);

                validarMasPaginas();

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {

            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosClientes adapter=new AdapterDatosClientes(listaClientes);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+listaClientes.get(recycler.getChildAdapterPosition(v)).getName(),
                        Toast.LENGTH_SHORT).show();
                clienteSelect=listaClientes.get(recycler.getChildAdapterPosition(v));

                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------
            }
        });
        //-----------------------------
        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    public void actionModificar(View view) {
        if(clienteSelect!=null){
            Intent winModificar=new Intent(this,ModClienActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("cliente",clienteSelect);
            winModificar.putExtras(bundle);
            winModificar.putExtra("URL",URL);

            startActivityForResult(winModificar,1);
            clienteSelect=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionBorrar(View view) {
     if(clienteSelect!=null){
        Intent winBorrar=new Intent(this,BorrClienActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("cliente",clienteSelect);
        winBorrar.putExtras(bundle);
        winBorrar.putExtra("URL",URL);

        startActivityForResult(winBorrar,1);
        clienteSelect=null;
         }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionRecargarClientes() {
        listaClientes.clear();
        obtenerClientes();
    }

    public void actionAgregar(View view) {
        Intent winAgregar=new Intent(this,AgClienActivity.class);
        winAgregar.putExtra("URL",URL);
        startActivityForResult(winAgregar,1);
    }

    public void actionRegresar(View view) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actionRecargarClientes();
    }

    private void validarMasPaginas() {
        if(listaClientes.size()==cantOrdenesAmostrar)
            masPaginas=true;
        else
            masPaginas=false;

        validarBtnNavOrdenes();
    }
    private void validarBtnNavOrdenes() {

        if(numPagina==0)
            btnAnterior.setEnabled(false);
        else
            btnAnterior.setEnabled(true);

        if(masPaginas)
            btnSiguiente.setEnabled(true);
        else
            btnSiguiente.setEnabled(false);

    }
    public void actionSiguiente(View view) {
        if(masPaginas){
            numPagina++;
            obtenerClientes();

        }
    }
    public void actionAnterior(View view) {
        if(numPagina>0){
            numPagina--;
            obtenerClientes();
        }
    }

    public void actionConsultarOrdenes(View view) {
        if(clienteSelect!=null){
            Intent ventana=new Intent(this,ConsultasOrdenesActivity.class);
            ventana.putExtra("URL",URL);
            ventana.putExtra("NivelAcceso",0);
            ventana.putExtra("idCliente",clienteSelect.getId());
            startActivity(ventana);
        }else{
            Toast.makeText(this, "Debe seleccionar un cliente antes.", Toast.LENGTH_SHORT).show();
        }
    }
}//end
