package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.AppUserApi;
import com.inyematic.Modelo.AppUser;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrCuenActivity extends AppCompatActivity {
    //---------------------
    private String URL;
    //---------------------
    TextView lblNombreCuenta;
    //---------------------
    AppUser appUser=null;
    //---------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_cuen);
        //--------------------
        URL=getIntent().getStringExtra("URL");
        appUser=(AppUser) getIntent().getSerializableExtra("cuenta");
        //------------------------------------
        lblNombreCuenta=(TextView)findViewById(R.id.lblNombreCuenta);
        lblNombreCuenta.setText(appUser.getUserName());
        //------------------------------------

    }

    public void actionBorrar(View view) {
        //------------------------------
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AppUserApi materialApi=retrofit.create(AppUserApi.class);
        //------------------------------
        Call<ResponseBody> call=materialApi.deleteAppUser(appUser.getId());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),"Se borro la cuenta",
                                Toast.LENGTH_LONG).show();
                        break;
                    case 404:Toast.makeText(getApplicationContext(),"No se puede borrar",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }
}//end
