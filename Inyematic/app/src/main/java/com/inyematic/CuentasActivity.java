package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosCuentas;
import com.inyematic.Interfaz.AppUserApi;
import com.inyematic.Modelo.AppUser;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CuentasActivity extends AppCompatActivity {
    AppUser auSelect=null;
    ArrayList<AppUser> listaCuentas=new ArrayList<>();
    //----------------------
    RecyclerView recycler;
    //----------------------
    private String URL;
    //-------------------------
    TextView selectPosRecy=null;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas);
        //---------------------------------
        recycler=(RecyclerView) findViewById(R.id.listaCuentas);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        obtenerCuentas();
    }
    private void recargarCuentas(){
        listaCuentas.clear();
        obtenerCuentas();
    }

    private void obtenerCuentas() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AppUserApi appUserApi=retrofit.create(AppUserApi.class);

        Call<List<AppUser>>call=appUserApi.getAllAppUser();

        call.enqueue(new Callback<List<AppUser>>() {
            @Override
            public void onResponse(Call<List<AppUser>> call, Response<List<AppUser>> response) {

                List<AppUser> lista = response.body();
                for (AppUser au : lista)
                    listaCuentas.add(au);

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<AppUser>> call, Throwable t) {

            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosCuentas adapter=new AdapterDatosCuentas(listaCuentas);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auSelect=listaCuentas.get(recycler.getChildAdapterPosition(v));
                Toast.makeText(getApplicationContext(),
                        "Selecciono:"+auSelect.getUserName(),
                        Toast.LENGTH_SHORT).show();

                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------


            }
        });
        recycler.setAdapter(adapter);
        Toast.makeText(getApplicationContext(),"Lista actualizada",
                Toast.LENGTH_SHORT).show();
    }

    public void actionAgregar(View view) {
        Intent ventana=new Intent(this,AgCuenActivity.class);
        ventana.putExtra("URL",URL);
        startActivityForResult(ventana,1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        recargarCuentas();
    }

    public void actionRegresar(View view) {
        finish();
    }

    public void actionModificar(View view){
        if(auSelect!=null){
            Intent ventana=new Intent(this,ModCuenActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("cuenta",auSelect);

            ventana.putExtras(bundle);
            ventana.putExtra("URL",URL);

            startActivityForResult(ventana,1);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Seleccione uno",
                    Toast.LENGTH_SHORT).show();
        }
    }
    public void actionBorrar(View view){
        if(auSelect!=null){
            Intent ventana=new Intent(this,BorrCuenActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("cuenta",auSelect);

            ventana.putExtra("URL",URL);
            ventana.putExtras(bundle);
            startActivityForResult(ventana,1);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Seleccione uno",
                    Toast.LENGTH_SHORT).show();
        }
    }
}//end
