package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MaterialApi;
import com.inyematic.Interfaz.MouldApi;
import com.inyematic.Modelo.Material;
import com.inyematic.Modelo.Molde;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgMoldActivity extends AppCompatActivity {

    ArrayList<Material> materiales=new ArrayList<>();
    Material matSelected;
    //-----------
    Spinner spinnerMat;
    EditText txtNombre;
    EditText txtPeso;
    TextView txtMatSelected;
    //----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_mold);
        //---------
        spinnerMat=(Spinner)findViewById(R.id.spinnerMat);
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtPeso=(EditText)findViewById(R.id.txtPeso);

        txtMatSelected=(TextView)findViewById(R.id.txtMatSelected);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------

        this.cargarMateriales();
    }

    private void cargarSpinner() {
       //*****Aca cargo el spinner con los datos del Array materiales
        ArrayAdapter adapter=new ArrayAdapter<Material>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                materiales);
        spinnerMat.setAdapter(adapter);

        //******Aca manejo la selección de un material dentro del spinner
        spinnerMat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                matSelected= (Material) parent.getItemAtPosition(position);
                txtMatSelected.setText(matSelected.getName());

                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+matSelected.getName(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarMateriales() {
        //******Cargo el array materiales****
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MaterialApi materialApi=retrofit.create(MaterialApi.class);

        Call<List<Material>> call=materialApi.getAllMaterials();

        call.enqueue(new Callback<List<Material>>() {
            @Override
            public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"Nose pudo obtener los materiales",Toast.LENGTH_SHORT).show();
                    return;
                }
                //*******
                List<Material> lstMaterials=response.body();

                for(Material m:lstMaterials){
                    materiales.add(m);
                }

                //Aca cargo el spinner. Esto es por que es asincrono.
                cargarSpinner();
            }

            @Override
            public void onFailure(Call<List<Material>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error en la conexión",Toast.LENGTH_SHORT).show();
            }
        });



    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionCargarMolde(View view) {
        if(validar()) {


            Molde newMould = new Molde(txtNombre.getText().toString(),
                    Float.valueOf(txtPeso.getText().toString()),
                    matSelected);

            //************************************
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            MouldApi mouldApi = retrofit.create(MouldApi.class);

            Call<ResponseBody> call = mouldApi.saveMould(newMould);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int codigo = response.code();

                    switch (codigo) {
                        case 200:
                            Toast.makeText(getApplicationContext(), "Se guardo ", Toast.LENGTH_SHORT).show();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(), "Error en los datos", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),
                            "Error en la conexión", Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toast.makeText(this, "Controle, faltan datos.", Toast.LENGTH_SHORT).show();
    }

    private boolean validar() {
        boolean bandera=true;

        bandera=(txtNombre.getText().toString().equals(""))?false:bandera;

        bandera=(Float.valueOf(txtPeso.getText().toString())==0)?false:bandera;

        return bandera;
    }
}//end
