package com.inyematic;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosCajas;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.Caja;
import com.inyematic.Modelo.OrdenProduccion;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AsigCajaActivity extends AppCompatActivity {
    //--------------------
    private String URL;
    //----------------------
    RecyclerView recycler;
    EditText txtCantPiezas;
    TextView lblOrdenNum,lblCantPiezas,lblCantPiezasAct;
    //--------------------
    OrdenProduccion orden;
    ArrayList<Caja> lstCajas=new ArrayList<>();
    Caja cajaSelect=null;
    //----------------------
    TextView selectPosRecy=null;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asig_caja);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.recyclerCajas);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        txtCantPiezas=(EditText)findViewById(R.id.txtCantPiezas);
        lblOrdenNum=(TextView)findViewById(R.id.lblOrdenNum);
        lblCantPiezas=(TextView)findViewById(R.id.lblCantPiezas);
        lblCantPiezasAct=(TextView)findViewById(R.id.lblCantPiezasAct);
        //----------
        obtenerOrden();
        //-----------
        obtenerCajas();
    }

    private void obtenerCajas() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
    //----------------------------------
        Call<List<Caja>>call =ordenProduccionApi.getAllDeliveryBoxFrom(orden.getId());

        call.enqueue(new Callback<List<Caja>>() {
            @Override
            public void onResponse(Call<List<Caja>> call, Response<List<Caja>> response) {
                List<Caja> lista=response.body();
                for(Caja c:lista)
                    lstCajas.add(c);

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<Caja>> call, Throwable t) {

            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosCajas adapter=new AdapterDatosCajas(lstCajas);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cajaSelect=lstCajas.get(recycler.getChildAdapterPosition(v));
                Toast.makeText(getApplicationContext(),
                        "Selecciono: Nº"+cajaSelect.getId(),
                        Toast.LENGTH_SHORT).show();

                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------


            }
        });




        recycler.setAdapter(adapter);

        //--------------------------------------------------------------------
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
        int suma=sumaPiezasCajas();
        lblCantPiezasAct.setText(String.valueOf(suma));
    }

    private int sumaPiezasCajas() {
        int resultado=0;
        for(Caja c:lstCajas)
            resultado+=c.getNumOfItems();
        return  resultado;
    }

    private void obtenerOrden() {
        //aca saco la orden que me envian en el Intent que "genera" esta ventana
        orden=(OrdenProduccion) getIntent().getExtras().getSerializable("orden");
        lblOrdenNum.setText(""+orden.getId());
        lblCantPiezas.setText(""+orden.getPieceCount());
    }

    public void actionRegresar(View view) {finish();
    }

    public void actionNuevaCaja(View view) {

        int cantidad=Integer.parseInt(txtCantPiezas.getText().toString());

        if(cantidad>0){

            Caja caja=new Caja();
            caja.setNumOfItems(cantidad);
            //--------------------
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
            //----------------------------------
            Call<ResponseBody> call=ordenProduccionApi.saveBoxToProductionOrder(caja,orden.getId());

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        Toast.makeText(getApplicationContext(),
                                "Se guardo la caja", Toast.LENGTH_SHORT).show();
                        actualizarCajas();

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }else{
            Toast.makeText(getApplicationContext(),
                    "La cantidad debe de ser mayor",
                    Toast.LENGTH_SHORT).show();
        }
    }
    private void actualizarCajas(){
        lstCajas.clear();
        obtenerCajas();
    }

    public void actionRemoverCaja(View view) {
        if(cajaSelect!=null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

            Call<ResponseBody>call =ordenProduccionApi.deleteDeliveryBoxFrom(orden.getId(),cajaSelect.getId());

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    int codigo=response.code();

                    switch (codigo){
                        case 200:
                            Toast.makeText(getApplicationContext(), "Se borro ", Toast.LENGTH_SHORT).show();
                            actualizarCajas();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(), "No se pudo borrar", Toast.LENGTH_SHORT).show();
                            break;
                        case 400:
                            Toast.makeText(getApplicationContext(), "Error en los datos", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar una caja",
                    Toast.LENGTH_SHORT).show();
        }
    }
}//end
