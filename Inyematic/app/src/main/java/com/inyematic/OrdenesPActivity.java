package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosOrdenesP;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.OrdenProduccion;
import com.inyematic.Modelo.StatusPO;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrdenesPActivity extends AppCompatActivity {
    //----------------------
    private String URL;
    //----------------------
    ArrayList<OrdenProduccion> listaOP=new ArrayList<>();
    OrdenProduccion opSelect;
    //-----------------
    RecyclerView recycler;
    int numPagina=0;
    int cantOrdenesAmostrar=10;
    boolean masPaginas=false;
    //--------------------

    Button btnAgregarOrden,btnSiguiente,btnAnterior;
    //--------------------
    Spinner spEstadoOrdenes;
    ArrayList<StatusPO>lstEstados=new ArrayList<>();
    //--------------------
    int NivelAcceso;
    //----------------------
    TextView selectPosRecy=null;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordenes_p);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        NivelAcceso=getIntent().getIntExtra("NivelAcceso",0);
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        spEstadoOrdenes=(Spinner)findViewById(R.id.spEstadoOrdenes);
        btnAgregarOrden=(Button)findViewById(R.id.btnAgregarOrden);
        btnSiguiente=(Button)findViewById(R.id.btnSiguiente);
        btnAnterior=(Button)findViewById(R.id.btnAnterior);
        //-------------------------------------
        obtenerListadoEstados();
        //actionRecargarOrdenes();
        setearCamposSegunNivelAcceso();
    }

    private void obtenerListadoEstados() {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<List<StatusPO>> call=ordenProduccionApi.getAllStatus();

        call.enqueue(new Callback<List<StatusPO>>() {
            @Override
            public void onResponse(Call<List<StatusPO>> call, Response<List<StatusPO>> response) {
                List<StatusPO> lista=response.body();

                for(StatusPO s:lista)
                    lstEstados.add(s);

                cargarSpinnerEstados();
            }

            @Override
            public void onFailure(Call<List<StatusPO>> call, Throwable t) {

            }
        });
    }

    private void cargarSpinnerEstados() {
        ArrayAdapter<CharSequence> adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,lstEstados);
        spEstadoOrdenes.setAdapter(adapter);

        spEstadoOrdenes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        obtenerOP();
                        break;
                    case 1:
                        obtenerOT();
                        break;
                    case 2:
                        obtenerOC();
                        break;
                    case 3:
                        obtenerOE();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void obtenerOE(){
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
        //---------------------
        Call<List<OrdenProduccion>> call=ordenProduccionApi.getPOdelivered(numPagina,cantOrdenesAmostrar);
        call.enqueue(new Callback<List<OrdenProduccion>>() {
            @Override
            public void onResponse(Call<List<OrdenProduccion>> call, Response<List<OrdenProduccion>> response) {
                List<OrdenProduccion> lista=response.body();
                listaOP.clear();
                for(OrdenProduccion op:lista)
                    listaOP.add(op);

                validarMasPaginas();

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<OrdenProduccion>> call, Throwable t) {

            }
        });
    }

    private void obtenerOC() {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
        //---------------------
        Call<List<OrdenProduccion>> call=ordenProduccionApi.getPOcanceled(numPagina,cantOrdenesAmostrar);
        call.enqueue(new Callback<List<OrdenProduccion>>() {
            @Override
            public void onResponse(Call<List<OrdenProduccion>> call, Response<List<OrdenProduccion>> response) {
                List<OrdenProduccion> lista=response.body();
                listaOP.clear();
                for(OrdenProduccion op:lista)
                    listaOP.add(op);

                validarMasPaginas();

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<OrdenProduccion>> call, Throwable t) {

            }
        });
    }

    private void setearCamposSegunNivelAcceso() {
        if(NivelAcceso==3){
           // btnBorrarOrden.setEnabled(false);
            btnAgregarOrden.setEnabled(false);
        }
    }

    private void obtenerOP() {
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);
        //---------------------
        Call<List<OrdenProduccion>> call=ordenProduccionApi.getPOproduction(numPagina,cantOrdenesAmostrar);

        call.enqueue(new Callback<List<OrdenProduccion>>() {
            @Override
            public void onResponse(Call<List<OrdenProduccion>> call, Response<List<OrdenProduccion>> response) {
                List<OrdenProduccion> lista=response.body();
                listaOP.clear();
                for(OrdenProduccion op:lista)
                    listaOP.add(op);

                validarMasPaginas();

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<OrdenProduccion>> call, Throwable t) {

            }
        });
    }

    private void validarMasPaginas() {
        if(listaOP.size()==cantOrdenesAmostrar)
            masPaginas=true;
        else
            masPaginas=false;

        validarBtnNavOrdenes();
    }

    private void validarBtnNavOrdenes() {

        if(numPagina==0)
            btnAnterior.setEnabled(false);
        else
            btnAnterior.setEnabled(true);

        if(masPaginas)
             btnSiguiente.setEnabled(true);
           else
             btnSiguiente.setEnabled(false);

    }

    private void obtenerOT(){
        Retrofit retrofit=obtenerRetro();
        OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

        Call<List<OrdenProduccion>>call=ordenProduccionApi.getPOfinished(numPagina,cantOrdenesAmostrar);

        call.enqueue(new Callback<List<OrdenProduccion>>() {
            @Override
            public void onResponse(Call<List<OrdenProduccion>> call, Response<List<OrdenProduccion>> response) {
                List<OrdenProduccion> lista=response.body();
                listaOP.clear();
                for(OrdenProduccion op:lista)
                    listaOP.add(op);

                validarMasPaginas();

                cargarRecycler();
            }

            @Override
            public void onFailure(Call<List<OrdenProduccion>> call, Throwable t) {

            }
        });
    }

    private void cargarRecycler() {
        AdapterDatosOrdenesP adapter=new AdapterDatosOrdenesP(listaOP);
        //----------------
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: Orden Nº"+
                                listaOP.get(recycler.getChildAdapterPosition(v)).getId(),
                        Toast.LENGTH_LONG).show();
                opSelect=listaOP.get(recycler.getChildAdapterPosition(v));
                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------
            }
        });

        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    private Retrofit obtenerRetro() {
        return   new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void actionAgregar(View view) {
        Intent winAgregar=new Intent(this,AgOrdenActivity.class);
        winAgregar.putExtra("URL",URL);
        startActivityForResult(winAgregar,1);
    }

    public void actionVerOrden(View view) {
       if(opSelect!=null){

            Intent winModificar =new Intent(this, ConsultarOrdenActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("ordenProduccion",opSelect);
            winModificar.putExtras(bundle);
            winModificar.putExtra("NivelAcceso",NivelAcceso);
            winModificar.putExtra("URL",URL);

            startActivityForResult(winModificar,1);
            opSelect=null;
       }else{
           Toast.makeText(getApplicationContext(),
                   "Debe seleccionar uno",
                   Toast.LENGTH_SHORT).show();
       }
    }

    public void actionRegresar(View view) {
        finish();
    }

    public void actionRecargarOrdenes() {
        listaOP.clear();
        spEstadoOrdenes.setSelection(0);
        obtenerOP();

    }

    public void actionWinCajas(View view) {

        if(opSelect!=null){
            if(opSelect.getStatus().getId()==1L){
                Intent ventana=new Intent(this,AsigCajaActivity.class);

                ventana.putExtra("URL",URL);

                Bundle bundle=new Bundle();
                bundle.putSerializable("orden",opSelect);
                ventana.putExtras(bundle);

                startActivityForResult(ventana,1);
                opSelect=null;
            }else{
                Toast.makeText(getApplicationContext(),
                        "No se puede cargar una caja",
                        Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar uno",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actionRecargarOrdenes();
    }

    public void actionBorrarOrden(View view) {
        if(opSelect!=null){
            Intent ventana=new Intent(this,BorrOrdenActivity.class);
            ventana.putExtra("URL",URL);

            Bundle bundle=new Bundle();
            bundle.putSerializable("orden",opSelect);
            ventana.putExtras(bundle);

            startActivityForResult(ventana,1);
            opSelect=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar uno",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionWinConsultas(View view) {
        Intent ventana=new Intent(this,ConsultasOrdenesActivity.class);
        ventana.putExtra("URL",URL);
        ventana.putExtra("NivelAcceso",NivelAcceso);
        startActivity(ventana);
    }

    public void actionSiguiente(View view) {
        if(masPaginas){
            numPagina++;
           ejecutarConsulta();

        }
    }
    public void actionAnterior(View view) {
        if(numPagina>0){
            numPagina--;
            ejecutarConsulta();
        }
    }

    private void ejecutarConsulta(){
        int posicion=spEstadoOrdenes.getSelectedItemPosition();
        switch (posicion){
            case 0:
                obtenerOP();
                break;
            case 1:
                obtenerOT();
                break;
            case 2:
                obtenerOC();
                break;
            case 3:
                obtenerOE();
                break;
        }
    }


}//end
