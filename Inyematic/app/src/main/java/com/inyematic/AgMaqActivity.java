package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Modelo.Maquina;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgMaqActivity extends AppCompatActivity {

    private String URL;
    //------------------
    EditText txtNombre,txtAlias,txtTolva;
    //----------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_maq);
        //-----------------------------
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtAlias=(EditText)findViewById(R.id.txtAlias);
        txtTolva=(EditText)findViewById(R.id.txtTolva);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
    }

    public void actionAgregar(View view) {
        if(validar()) {


            Maquina maquina = new Maquina();
            maquina.setName(txtNombre.getText().toString());
            maquina.setAlias(txtAlias.getText().toString());
            maquina.setHopperSize(Integer.parseInt(txtTolva.getText().toString()));

            //-----------------
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            MaquinaApi maquinaApi = retrofit.create(MaquinaApi.class);
            //----------------
            Call<ResponseBody> call = maquinaApi.saveMachine(maquina);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int op = response.code();
                    switch (op) {
                        case 200:
                            Toast.makeText(getApplicationContext(), "Se guardo la maquina", Toast.LENGTH_SHORT).show();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(), "No se guardo la maquina", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),
                            "Error de conexión",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            Toast.makeText(this, "Controle, faltan datos", Toast.LENGTH_SHORT).show();
    }
    private boolean validar() {
        boolean bandera=true;

        bandera=(txtNombre.getText().toString().equals(""))?false:bandera;

        bandera=(txtAlias.getText().toString().equals(""))?false:bandera;

        bandera=(Integer.parseInt(txtTolva.getText().toString())==0)?false:bandera;

        return bandera;
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

}//end
