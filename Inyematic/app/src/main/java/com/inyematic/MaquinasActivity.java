package com.inyematic;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.AdapterDatos.AdapterDatosMaquinas;
import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Modelo.Maquina;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MaquinasActivity extends AppCompatActivity {

    private String URL;
    //------------------
    ArrayList<Maquina> listaMaquinas=new ArrayList<>();
    Maquina maquinaSelect;
    //-------------------
    RecyclerView recycler;
    //-------------------------
    TextView selectPosRecy=null;
    //---------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maquinas);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        recycler=(RecyclerView)findViewById(R.id.listaDatos);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        //-----------------------------------
        obtenerLasMaquinas();
    }

    private void obtenerLasMaquinas() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MaquinaApi maquinaApi=retrofit.create(MaquinaApi.class);
        //--------------------------------
        Call<List<Maquina>> call=maquinaApi.getAllMachines();

        call.enqueue(new Callback<List<Maquina>>() {
            @Override
            public void onResponse(Call<List<Maquina>> call, Response<List<Maquina>> response) {
                if(response.isSuccessful()){
                    List<Maquina> lista=response.body();
                    for(Maquina m:lista)
                        listaMaquinas.add(m);
                }
                cargarListado();
            }

            @Override
            public void onFailure(Call<List<Maquina>> call, Throwable t) {

            }
        });
    }

    private void cargarListado() {
        AdapterDatosMaquinas adapter=new AdapterDatosMaquinas(listaMaquinas);
        //-------------------
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Selecciono: "+
                                listaMaquinas.get(recycler.getChildAdapterPosition(v)).getName(),
                        Toast.LENGTH_LONG).show();
                maquinaSelect=listaMaquinas.get(recycler.getChildAdapterPosition(v));

                //------------------------------------------
                //Aca obtengo el holder del v, que me va a dar acceso al idDato
                TextView dato=(TextView)recycler.getChildViewHolder(v).itemView.findViewById(R.id.idDato);

                if(selectPosRecy==null){
                    selectPosRecy=dato;
                    dato.setTextColor(Color.WHITE);
                }else{
                    if(dato!=selectPosRecy){
                        selectPosRecy.setTextColor(Color.BLACK);
                        dato.setTextColor(Color.WHITE);
                        selectPosRecy=dato;
                    }
                }
                //------------------------------------------------------

            }
        });
        //--------------------
        recycler.setAdapter(adapter);
        Toast.makeText(this,"Lista Actualizada",Toast.LENGTH_SHORT).show();
    }

    public void actionBorrar(View view) {
        if(maquinaSelect!=null){
                Intent winBorrar=new Intent(this,BorrMaqActivity.class);

                Bundle bundle=new Bundle();
                bundle.putSerializable("maquina",maquinaSelect);

                winBorrar.putExtras(bundle);
                winBorrar.putExtra("URL",URL);

                startActivityForResult(winBorrar,1);
                maquinaSelect=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionModificar(View view) {
        if(maquinaSelect!=null){
            Intent winModificar=new Intent(this,ModMaqActivity.class);

            Bundle bundle=new Bundle();
            bundle.putSerializable("maquina",maquinaSelect);

            winModificar.putExtras(bundle);
            winModificar.putExtra("URL",URL);

            startActivityForResult(winModificar,1);
            maquinaSelect=null;
        }else{
            Toast.makeText(getApplicationContext(),
                    "Debe seleccionar antes",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void actionRecargarMaquinas() {
        listaMaquinas.clear();
        obtenerLasMaquinas();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actionRecargarMaquinas();
    }

    public void actionAgregar(View view) {

        Intent winAgregar=new Intent(this,AgMaqActivity.class);
        winAgregar.putExtra("URL",URL);
        startActivityForResult(winAgregar,1);

    }

    public void actionRegresar(View view) {
        finish();
    }
}//end
