package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Modelo.Maquina;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BorrMaqActivity extends AppCompatActivity {
    //----------
    private String URL;
    //------------------
    TextView txtNombre;
    //---------------------
    Maquina maquina;
    //---------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borr_maq);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        //-----------------
        obtenerMaquina();
    }

    private void obtenerMaquina() {
        Bundle bundle=getIntent().getExtras();
        maquina=(Maquina)bundle.getSerializable("maquina");
        txtNombre.setText(maquina.getName());
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionBorrar(View view) {
        //------------------------------
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MaquinaApi maquinaApi=retrofit.create(MaquinaApi.class);
        //------------------------------
        Call<ResponseBody> call=maquinaApi.deleteMachine(maquina.getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                switch (response.code()){
                    case 200:
                        Toast.makeText(getApplicationContext(),
                                "Se borro correctamente",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 404:
                        Toast.makeText(getApplicationContext(),
                                "No se pudo borrar",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}//end
