package com.inyematic;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.OrdenProduccion;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GraficoOrdenesActivity extends AppCompatActivity {
    BarChart barChart;
    //-------------------
    ArrayList<OrdenProduccion> listaOrdenes;
    ArrayList<Integer> listaCantidad=new ArrayList<>();
    ArrayList<String> listaOp=new ArrayList<String>();
    ArrayList<Integer> listaCantidadReal=new ArrayList<>();
    private String URL;
    //-------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico_ordenes);
        //--------------------------
        barChart=(BarChart)findViewById(R.id.barChart);
        //--------------------
        obtenerDatos();
        //Toast.makeText(this, "pedi:"+listaCantidad.size()+"real:"+listaCantidadReal.size(), Toast.LENGTH_SHORT).show();
       // createCharts();
        cargarChart();
    }

    private void cargarChart() {
        //seteo inicial
       /* barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setMaxVisibleValueCount(listaOp.size());
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);
       */ //-----------------------
        //cargamos los BarEntry que seran los datos a mostrar
        //primero el de las cantidades pedidas
        ArrayList<BarEntry> CantPedida=obtenerListBarEntry(listaCantidad);

        //cantidades reales
        ArrayList<BarEntry> CantReal=obtenerListBarEntry(listaCantidadReal);

        barChart.setDrawGridBackground(true);

        //-------------------------------------------------
        //creamos los BarDataSet
        BarDataSet barDataSetCantPedida=new BarDataSet(CantPedida,"Pedido");
        BarDataSet barDataSetCantReal=new BarDataSet(CantReal,"Real");

        //le asignamos un color a cada bardataset
        barDataSetCantPedida.setColor(Color.CYAN);
        barDataSetCantReal.setColor(Color.RED);

        barDataSetCantPedida.setValueTextSize(10f);
        barDataSetCantReal.setValueTextSize(10f);

        //-----------------------------------------------
        //ahora creamos el BarData
        BarData datos=new BarData();
        datos.addDataSet(barDataSetCantReal);
        datos.addDataSet(barDataSetCantPedida);



        //finalmente cargamos el BarData al chart
        barChart.setData(datos);
        barChart.setHorizontalScrollBarEnabled(true);
        //-------------------------
        XAxis xAxis=barChart.getXAxis();

        //----------------------------------------------------


        xAxis.setValueFormatter(new IndexAxisValueFormatter(listaOp));

        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);
        //-----------------------------------

        barChart.setDragEnabled(true);
        barChart.setVisibleXRangeMinimum(3);

        int numeroDeBarras=2;
        float barWidth=0.1f;
        float barSpace=0.1f;

        float groupSpace=1-((barWidth+barSpace)*numeroDeBarras);

        datos.setBarWidth(barWidth);
        barChart.getXAxis().setAxisMinimum(0);
        barChart.getXAxis().setAxisMaximum(0+barChart.getBarData().getGroupWidth(groupSpace,barSpace)*(listaOrdenes.size()));
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisRight().setEnabled(false);
        barChart.groupBars(0,groupSpace,barSpace);



        barChart.invalidate();

    }



    private ArrayList<BarEntry> obtenerListBarEntry(ArrayList<Integer> lista) {
        ArrayList<BarEntry> resultado=new ArrayList<>();
        for(int i=0;lista.size()>i;i++){
            resultado.add(new BarEntry(i,lista.get(i)));
        }
        return resultado;
    }


    /*
        private Chart getSameChart(Chart chart,String description,int textColor, int background,int animateY ){
            chart.getDescription().setText(description);
            chart.getDescription().setTextSize(10);
            chart.setBackgroundColor(background);
            chart.animateY(animateY);
            legend(chart);
    
            return chart;
        }
        private void legend(Chart chart){
            //general
            Legend legend=chart.getLegend();
            legend.setForm(Legend.LegendForm.SQUARE);
            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
    
            //cada una de las entradas de la leyenda
            ArrayList<LegendEntry>entries=new ArrayList<>();
            LegendEntry legendEntry=new LegendEntry();
            legendEntry.formColor= Color.CYAN;
            legendEntry.label="Cant. pedida";
    
            LegendEntry legendEntry1=new LegendEntry();
            legendEntry1.formColor=Color.RED;
            legendEntry1.label="Cant. real";
    
            entries.add(legendEntry);
            entries.add(legendEntry1);
    
            legend.setCustom(entries);
        }
    
        private ArrayList<BarEntry> getBarEntries(){
            ArrayList<BarEntry> entries=new ArrayList<>();
            for(Integer cant:listaCantidad){
                entries.add(new BarEntry(listaCantidad.indexOf(cant),cant));
            }
            return entries;
        }
        private void axisX(XAxis axis){
            axis.setGranularityEnabled(true);
            axis.setGranularity(1);
            axis.setAxisMinimum(1);
            axis.setPosition(XAxis.XAxisPosition.BOTTOM);
            axis.setValueFormatter(new IndexAxisValueFormatter(listaOp));
        }
    
        private void axisLeft(YAxis axis){
            axis.setSpaceTop(5f);
            axis.setAxisMinimum(0f);
        }
    
        private void axisRight(YAxis axis){
            axis.setEnabled(false);
        }
    
        private void createCharts(){
            barChart=(BarChart) getSameChart(barChart,"Ordenes",Color.BLACK,Color.YELLOW,3000);
            barChart.setDrawGridBackground(true);
            barChart.setData(getBarData());
            barChart.invalidate();
            //--------------------
            float groupSpace=0.1f;
            float barSpace=0.02f;
            barChart.groupBars(1,groupSpace,barSpace);
            //-------------
    
            axisX(barChart.getXAxis());
            axisLeft(barChart.getAxisLeft());
            axisRight(barChart.getAxisRight());
        }
    
        private DataSet getData(DataSet dataSet){
    
                dataSet.setValueTextColor(Color.BLACK);
                dataSet.setValueTextSize(10);
    
            return dataSet;
        }
    
        private BarData getBarData(){
            BarDataSet barDataSet=(BarDataSet)getData(new BarDataSet(getBarEntries(),""));
            barDataSet.setBarShadowColor(Color.GRAY);
    
            BarDataSet barDataSet1=(BarDataSet)getData(new BarDataSet(getBarEntries1(),""));
            barDataSet1.setBarShadowColor(Color.GRAY);
            barDataSet1.setColor(Color.RED);
    
            BarData barData=new BarData(barDataSet,barDataSet1);
    
    
            float barWidth=0.43f;
    
            barData.setBarWidth(barWidth);
    
            return barData;
        }
    
        private List<BarEntry> getBarEntries1() {
            ArrayList<BarEntry> entries=new ArrayList<>();
    
            for(Integer cant:listaCantidadReal)
                entries.add(new BarEntry(listaCantidadReal.indexOf(cant),cant));
    
            return entries;
        }
    
    
    */
    private void obtenerDatos() {
        URL=getIntent().getStringExtra("URL");
        //----------------
       listaOrdenes=(ArrayList<OrdenProduccion>)getIntent().getSerializableExtra("lista");
       for (OrdenProduccion op:listaOrdenes){
           listaCantidad.add(op.getPieceCount());
           listaOp.add("Nº "+op.getId());
       }
        listaCantidadReal=(ArrayList<Integer>)getIntent().getSerializableExtra("listaCantReal");

    }
    public void actionRegresar(View view){
        finish();
    }
}//end
