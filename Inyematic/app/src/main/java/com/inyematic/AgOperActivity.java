package com.inyematic;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Modelo.Operador;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgOperActivity extends AppCompatActivity {

    EditText txtNombre;
    TextView txtDate;
    EditText txtAddress;
    Button btnFecha;
    //------------
    Date fecha;

    //----------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_oper);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtDate=(TextView)findViewById(R.id.txtDate);
        txtAddress=(EditText)findViewById(R.id.txtAddress);
        btnFecha=(Button)findViewById(R.id.btnFecha);
        //--------------------
        Calendar hoy=Calendar.getInstance();

        actualizarFecha(hoy.getTime());
    }

    private void actualizarFecha(Date fecha){
        this.fecha=(Date) fecha.clone();
        SimpleDateFormat formateador=new SimpleDateFormat("dd-MM-yyyy");
        String salida=formateador.format(fecha.getTime());
        txtDate.setText(salida);

    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionAgregar(View view) {
        if(validar()) {


            Operador operador = new Operador();
            operador.setName(txtNombre.getText().toString());
            operador.setBirthDate(this.fecha);
            operador.setAddress(txtAddress.getText().toString());


            //-----------------------------------------
            GsonBuilder gb = new GsonBuilder();
            gb.setDateFormat("dd-MM-yyyy");

            Gson gson = gb.create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            //---------------------------------------------
            OperadorApi operadorApi = retrofit.create(OperadorApi.class);

            Call<ResponseBody> call = operadorApi.saveOperator(operador);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    switch (response.code()) {
                        case 200:
                            Toast.makeText(getApplicationContext(),
                                    "Se guardo el operador",
                                    Toast.LENGTH_LONG).show();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(), "No se pudo guardar",
                                    Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error de conexión", Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toast.makeText(this, "Controle, faltan datos", Toast.LENGTH_SHORT).show();

    }
    private boolean validar() {
        boolean bandera=true;

        bandera=(txtNombre.getText().toString().equals(""))?false:bandera;

        bandera=(txtAddress.getText().toString().equals(""))?false:bandera;

        return bandera;
    }

    public void actionFecha(View view) {
        Intent winFecha=new Intent(this,CalendarioActivity.class);
        startActivityForResult(winFecha,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //-------------------
       if(resultCode == RESULT_CANCELED){
            Toast.makeText(this,"Resultado Cancelado",Toast.LENGTH_LONG).show();
        }else{
            int año=data.getExtras().getInt("año");
            int mes=data.getExtras().getInt("mes");
            int dia=data.getExtras().getInt("dia");
            Calendar calendar=Calendar.getInstance();
            calendar.set(año,mes,dia);
            this.actualizarFecha(calendar.getTime());
        }

    }

}//end
