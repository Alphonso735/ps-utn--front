package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Modelo.Cliente;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgClienActivity extends AppCompatActivity {

    TextView txtNombre,txtAddress,txtPhone;
    //--------------------
    private String URL;
    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_clien);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        txtAddress=(TextView)findViewById(R.id.txtAddress);
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        txtPhone=(TextView)findViewById(R.id.txtPhone);
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionAgregar(View view) {
        if(validar()) {


            Cliente cliente = new Cliente();
            cliente.setName(txtNombre.getText().toString());
            cliente.setAddress(txtAddress.getText().toString());
            cliente.setTelephone(Long.parseLong(txtPhone.getText().toString()));
            //-------------------------
            Retrofit retrofit = obtenerRetro();

            ClienteApi clienteApi = retrofit.create(ClienteApi.class);

            Call<ResponseBody> call = clienteApi.saveClient(cliente);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    switch (response.code()) {
                        case 200:
                            Toast.makeText(getApplicationContext(),
                                    "Se guardo el cliente",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case 404:
                            Toast.makeText(getApplicationContext(),
                                    "No se guardo el cliente",
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error de conexión", Toast.LENGTH_SHORT).show();

                }
            });
        }else
            Toast.makeText(this, "Controle, faltan datos.", Toast.LENGTH_SHORT).show();

    }

    private boolean validar() {
        boolean bandera=true;

        bandera=(txtNombre.getText().toString().equals(""))?false:bandera;

        bandera=(txtAddress.getText().toString().equals(""))?false:bandera;

        bandera=(Long.parseLong(txtPhone.getText().toString())==351)?false:bandera;

        return bandera;
    }

    private Retrofit obtenerRetro() {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

}//end
