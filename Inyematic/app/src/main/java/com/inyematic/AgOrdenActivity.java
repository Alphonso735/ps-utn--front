package com.inyematic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyematic.Interfaz.ClienteApi;
import com.inyematic.Interfaz.MaquinaApi;
import com.inyematic.Interfaz.MouldApi;
import com.inyematic.Interfaz.OperadorApi;
import com.inyematic.Interfaz.OrdenProduccionApi;
import com.inyematic.Modelo.Cliente;
import com.inyematic.Modelo.Maquina;
import com.inyematic.Modelo.Molde;
import com.inyematic.Modelo.Operador;
import com.inyematic.Modelo.OrdenProduccion;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.HTTP;

public class AgOrdenActivity extends AppCompatActivity {
    //----------------------
    private String URL;
    //----------------------
    ArrayList<Molde> lstMoldes=new ArrayList<>();
    Molde moldeSelect;
    Spinner spMoldes;
    TextView lblMolde;
    //-----------------------------------------
    ArrayList<Operador> lstOperadores=new ArrayList<>();
    Operador operdorSelect;
    Spinner spOperadores;
    TextView lblOperador;
    //------------------------------------------
    ArrayList<Maquina> lstMaquinas=new ArrayList<>();
    Maquina maquinaSelect;
    Spinner spMaquinas;
    TextView lblMaquina;
    //-------------------------------------------
    ArrayList<Cliente> lstClientes=new ArrayList<>();
    Cliente clienteSelect;
    Spinner spClientes;
    TextView lblCliente;
    //-------------------------------------------
    EditText txtCantPiezas;
    //------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ag_orden);
        //---------------------------------
        URL=getIntent().getStringExtra("URL");
        //---------------------------------
        spMoldes=(Spinner)findViewById(R.id.spMolde);
        spMaquinas=(Spinner)findViewById(R.id.spMaquina);
        spOperadores=(Spinner)findViewById(R.id.spOperador);
        spClientes=(Spinner)findViewById(R.id.spCliente);

        txtCantPiezas=(EditText)findViewById(R.id.txtCantPiezas);

        lblMolde=(TextView)findViewById(R.id.lblMolde);
        lblMaquina=(TextView)findViewById(R.id.lblMaquina);
        lblOperador=(TextView)findViewById(R.id.lblOperador);
        lblCliente=(TextView)findViewById(R.id.lblCliente);
        //---------------------------------
        cargarMoldes();
        cargarMaquinas();
        cargarOperadores();
        cargarClientes();
        //-----------------
        inicializarCampos();
    }

    private void inicializarCampos() {
        txtCantPiezas.setText("0");
    }

    private void cargarClientes() {
        Retrofit retrofit=obtenerRetro();
        ClienteApi clienteApi=retrofit.create(ClienteApi.class);

        Call<List<Cliente>> call=clienteApi.getAllClients();

        call.enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                List<Cliente> lista=response.body();

                for(Cliente c:lista)
                    lstClientes.add(c);

                cargarSpClientes();
            }

            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {

            }
        });

    }

    private void cargarSpClientes() {
        ArrayAdapter adapter=new ArrayAdapter<Cliente>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstClientes);
        spClientes.setAdapter(adapter);

        //--------------------------
        spClientes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clienteSelect=(Cliente)parent.getItemAtPosition(position);
                lblCliente.setText(clienteSelect.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarMoldes() {
        Retrofit retrofit=this.obtenerRetro();
        MouldApi mouldApi=retrofit.create(MouldApi.class);

        Call<List<Molde>> call=mouldApi.getAllMoulds();

        call.enqueue(new Callback<List<Molde>>() {
            @Override
            public void onResponse(Call<List<Molde>> call, Response<List<Molde>> response) {
                List<Molde> lista=response.body();
                for(Molde m:lista)
                    lstMoldes.add(m);
                cargarSpMoldes();
            }

            @Override
            public void onFailure(Call<List<Molde>> call, Throwable t) {

            }
        });
    }

    private void cargarSpMoldes() {
        ArrayAdapter adapter=new ArrayAdapter<Molde>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstMoldes);
        spMoldes.setAdapter(adapter);

        //---------------------------------
        spMoldes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                moldeSelect=(Molde)parent.getItemAtPosition(position);
                lblMolde.setText(moldeSelect.getName());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarMaquinas(){
        Retrofit retrofit=this.obtenerRetro();
        MaquinaApi maquinaApi=retrofit.create(MaquinaApi.class);
        Call<List<Maquina>> call=maquinaApi.getAllMachines();

        call.enqueue(new Callback<List<Maquina>>() {
            @Override
            public void onResponse(Call<List<Maquina>> call, Response<List<Maquina>> response) {
                List<Maquina> lista=response.body();
                for(Maquina m:lista)
                    lstMaquinas.add(m);
                cargarSpMaquinas();
            }

            @Override
            public void onFailure(Call<List<Maquina>> call, Throwable t) {

            }
        });
    }

    private void cargarSpMaquinas() {
        ArrayAdapter adapter=new ArrayAdapter<Maquina>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstMaquinas);
        spMaquinas.setAdapter(adapter);

        //*********************
        spMaquinas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maquinaSelect=(Maquina)parent.getItemAtPosition(position);
                lblMaquina.setText(maquinaSelect.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarOperadores(){
        Retrofit retrofit=obtenerRetro();
        OperadorApi operadorApi=retrofit.create(OperadorApi.class);

        Call<List<Operador>> call=operadorApi.getAllOperators();

        call.enqueue(new Callback<List<Operador>>() {
            @Override
            public void onResponse(Call<List<Operador>> call, Response<List<Operador>> response) {
                List<Operador> lista=response.body();
                for(Operador o:lista)
                    lstOperadores.add(o);

                cargarSpOperadores();
            }

            @Override
            public void onFailure(Call<List<Operador>> call, Throwable t) {

            }
        });
    }

    private void cargarSpOperadores() {
        ArrayAdapter adapter=new ArrayAdapter<Operador>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line,
                lstOperadores);
        spOperadores.setAdapter(adapter);

        //*********************
        spOperadores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                operdorSelect=(Operador)parent.getItemAtPosition(position);
                lblOperador.setText(operdorSelect.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

      private Retrofit obtenerRetro(){
        GsonBuilder gb = new GsonBuilder();
        gb.setDateFormat("dd-MM-yyyy");

        Gson gson=gb.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return  retrofit;
    }

    public void actionRegresar(View view) {
        setResult(1);
        finish();
    }

    public void actionGuardar(View view) {


        OrdenProduccion ordenProduccion=new OrdenProduccion();
        if(validarCampos()){
            ordenProduccion.setMachine(maquinaSelect);
            ordenProduccion.setMould(moldeSelect);
            ordenProduccion.setOperator(operdorSelect);
            ordenProduccion.setPieceCount(Integer.parseInt(txtCantPiezas.getText().toString()));
            ordenProduccion.setCliente(clienteSelect);
            //---------------------------------------------
            Retrofit retrofit=obtenerRetro();
            OrdenProduccionApi ordenProduccionApi=retrofit.create(OrdenProduccionApi.class);

            Call<ResponseBody> call=ordenProduccionApi.saveProductionOrder(ordenProduccion);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    int codigo=response.code();

                    switch (codigo){
                        case 200://ok
                            Toast.makeText(getApplicationContext(), "Se guardo ", Toast.LENGTH_SHORT).show();
                            break;
                        case 507://insuficiente material
                            Toast.makeText(getApplicationContext(), "Insuficiente material", Toast.LENGTH_SHORT).show();
                            break;
                        case 400://bad request
                            Toast.makeText(getApplicationContext(), "Error en los datos", Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error conexion", Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(getApplicationContext(),
                    "Controle, faltan datos",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private boolean validarCampos() {
        boolean result=true;

        result=(moldeSelect == null)?false:result;
        result=(maquinaSelect == null)?false:result;
        result=(operdorSelect == null)?false:result;
        result=(clienteSelect == null)?false:result;
        long cant= Long.parseLong(txtCantPiezas.getText().toString());
        result=(cant == 0f)?false:result;

        return result;
    }

}//end
